import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Reporting',
    type: 'group',
    icon: 'icon-navigation',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        icon: 'feather icon-home',
        url: '/dashboard/default',
        classes: 'nav-item' 
      }
    ]
  },
  {
    id: 'manager',
    title: 'Gestion',
    type: 'group',
    icon: 'icon-ui',
    children: [
      {
        id: 'cooperative',
        title: 'Cooperative',
        type: 'collapse',
        icon: 'fas fa-sitemap',
        children: [
          {
            id: 'cooperative-add',
            title: 'Ajouter une coopérative',
            type: 'item',
            url: '/cooperative/add'
          },
          {
            id: 'cooperative-list',
            title: 'Liste des coopératives',
            type: 'item',
            url: '/cooperative/list'
          },
          {
            id: 'cooperative-details',
            title: 'Information coopérative',
            type: 'item',
            url: '/cooperative/info'
          }
        ]
      },
      {
        id: 'member',
        title: 'Membre',
        type: 'collapse',
        icon: 'fas fa-handshake-o',
        children: [
          {
            id: 'member-add',
            title: 'Ajouter un membre',
            type: 'item',
            url: '/member/add'
          },
          {
            id: 'members-list',
            title: 'Liste des membres',
            type: 'item',
            url: '/member/list'
          }
        ]
      },
      {
        id: 'tontine',
        title: 'Tontine',
        type: 'collapse',
        icon: 'fas fa-wallet',
        children: [
          {
            id: 'tontine-add',
            title: 'Créer une tontine',
            type: 'item',
            url: '/tontine/add'
          },
          {
            id: 'tontine-list',
            title: 'Liste des Tontines',
            type: 'item',
            url: '/tontine/list'
          },
          {
            id: 'tontine-participant',
            title: 'Gestion des participants',
            type: 'item',
            url: '/tontine/participant'
          },
          {
            id: 'tontine-tours',
            title: 'Gestion des tours',
            type: 'item',
            url: '/tontine/tours'
          },
          {
            id: 'alert',
            title: 'Contributions',
            type: 'item',
            url: '/maintenance/coming-soon'
          },
          {
            id: 'alert',
            title: 'Information tontine',
            type: 'item',
            url: '/maintenance/coming-soon'
          }
        ]
      },
      
    ]
  },
  {
    id: 'forms',
    title: 'Paramétrage',
    type: 'group',
    icon: 'icon-group',
    children: [
      {
        id: 'country_town',
        title: 'Gestion des Pays',
        type: 'item',
        url: '/settings/country',
        icon: 'fas fa-flag',
        classes: 'nav-item'
      },
      {
        id: 'forms-element',
        title: 'Types tontine',
        type: 'item',
        url: '/settings/tontine',
        classes: 'nav-item' ,
        icon: 'feather icon-file-text'
      },
      {
        id: 'forms-element',
        title: 'Gestion des utilisateurs',
        type: 'item',
        url: '/settings/app',
        classes: 'nav-item' ,
        icon: 'feather icon-users'
      }
    ]
  },
  {
    id: 'session',
    title: 'Session',
    type: 'group',
    icon: 'icon-table',
    children: [
      {
        id: 'account',
        title: 'Mon compte',
        type: 'item',
        icon: 'feather icon-user-check',
        url: '/settings/user',
        classes: 'nav-item' 
      },
      {
        id: 'logout',
        title: 'Deconnexion',
        type: 'item',
        icon: 'feather icon-log-out',
        //url: '/De',
        classes: 'nav-item' 
      }
    ]
  }
];

@Injectable()
export class NavigationItem {
  get() {
    return NavigationItems;
  }
}
