import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TontineAddComponent} from './tontine-add.component';

const routes: Routes = [
  {
    path: '',
    component: TontineAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TontineAddRoutingModule { }
