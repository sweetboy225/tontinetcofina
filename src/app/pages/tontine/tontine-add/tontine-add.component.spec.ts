import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {TontineAddComponent } from './tontine-add.component';

describe('TontineAddComponent', () => {
  let component: TontineAddComponent;
  let fixture: ComponentFixture<TontineAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TontineAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TontineAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
