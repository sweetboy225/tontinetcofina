import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        loadChildren: () => import('./tontine-add/tontine-add.module').then(m => m.TontineAddModule)
      },
      {
        path: 'list',
        loadChildren: () => import('./tontine-list/tontine-list.module').then(m => m.TontineListModule)
      } ,
      {
        path: 'info',
        loadChildren: () => import('./tontine-info/tontine-info.module').then(m => m.TontineInfoModule)
      },
      {
        path: 'participant',
        loadChildren: () => import('./tontine-participant/tontine-participant.module').then(m => m.TontineParticipantModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TontineRoutingModule { }
  