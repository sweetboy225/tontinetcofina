import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TontineInfoComponent} from './tontine-info.component';

const routes: Routes = [
  {
    path: '',
    component: TontineInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TontineInfoRoutingModule { }
