import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {TontineInfoComponent } from './tontine-info.component';
describe('TontineInfoComponent', () => {
  let component: TontineInfoComponent;
  let fixture: ComponentFixture<TontineInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TontineInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TontineInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
