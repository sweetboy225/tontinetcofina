import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { GeneralService } from 'src/app/shared/services/general.service';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { TontineService } from 'src/app/shared/services/tontine.service';

import { Router, ActivatedRoute } from '@angular/router';
import { CooperativeModel } from 'src/app/shared/models/cooperative.model';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';
import { TontineTypeModel } from 'src/app/shared/models/tontineType.model';
import {Location} from '@angular/common';
import { ShareModel } from 'src/app/shared/models/share.model';
@Component({
  selector: 'app-tontine-info',
  templateUrl: './tontine-info.component.html',
  styleUrls: ['./tontine-info.component.scss']
})
export class TontineInfoComponent implements OnInit {
  asEdit= false;
  form: any;
  dayOfWeek= '';
  shareList: ShareModel[] = null;
  public isSubmit: boolean;
  tontineTypeList: TontineTypeModel[]
  closeResult = '';
  cooperatives: CooperativeModel[];
  cooperativeId= '';
  towns: TownModel[];
  userId= '';
  usersListEmpty = true;
  CultureNameEn= 'en-US';
  CultureNameFr= 'fr-FR';
  tontineId= '';
  enOn= false;
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private cooperative: CooperativeService,
    private tontine: TontineService,
    private router: Router,
    private activateRouter:ActivatedRoute,
    private _location: Location
    ) {
      this.userId = localStorage.getItem('userId');
      this.getCooperativesList();
      this.getTontineTypeList();
      this.isSubmit = false;
    }

  ngOnInit() {
    this.tontineId = sessionStorage.getItem('tontineId')
    console.log('Coop Id: ', this.tontineId ) 
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = true; 
  }

  langueBtn() {
    this.enOn = !this.enOn;
  }

  async getTownById(Id) {
    
  }
  async getTontineTypeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getTontineType())
    .toPromise()
    .then(async (res) => {
      this.tontineTypeList = res as TontineTypeModel[];
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async getCooperativesList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperatives = res as CooperativeModel[];
      console.log('Cooperatice: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Connexion impossible');
    });
  }
   
  async createTontine( tontineForm) {

    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    console.log('Create: ', tontineForm + ' ' + this.cooperativeId, + '' + tontineForm.Type);
    (await this.tontine.createTontine(tontineForm, this.cooperativeId, ))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Tontine crée avec succès', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/tontine/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }

  async generateShares() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.generateShares(this.tontineId, this.dayOfWeek, ))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Tours créé avec succès', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/tontine/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }

  async clearShares() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.clearShares(this.tontineId ))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Tours réinitialisé avec succès', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/tontine/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }
  async getShares() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getAllShares(this.tontineId ))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.shareList = res
     this.toastr.success('Tontine crée avec succès', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/tontine/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }

  async resetForm() {

  }

  prevButton() {
    this._location.back();
  }
}

