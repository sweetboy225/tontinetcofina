import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TontineInfoRoutingModule } from './tontine-info-routing.module';
import { TontineInfoComponent } from './tontine-info.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {CustomFormsModule} from 'ngx-custom-validators';

@NgModule({
  imports: [
    CommonModule,
   TontineInfoRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule,
    CustomFormsModule

  ],
  declarations: [TontineInfoComponent],
  providers: [DatePipe]
})
export class TontineInfoModule { } 
