import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TontineListComponent} from './tontine-list.component';


const routes: Routes = [
  { 
    path: '',
    component: TontineListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TontineListRoutingModule { }
