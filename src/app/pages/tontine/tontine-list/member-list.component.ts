import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Subject } from 'rxjs';

import { DatePipe } from '@angular/common';
import { SecurityModel } from './node_modules/src/app/shared/models/security.model';
import { RoleModel } from './node_modules/src/app/shared/models/role.model';
import { PermissionSetsModel } from './node_modules/src/app/shared/models/permissionSets.model';
import { SecurityService } from './node_modules/src/app/shared/services/security.service';

import './node_modules/sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { TontineTypeModel } from './node_modules/src/app/shared/models/tontineType.model';
import { TontineService } from './node_modules/src/app/shared/services/tontine.service';
import { MemberService } from './node_modules/src/app/shared/services/member.service';
import { MemberModel } from './node_modules/src/app/shared/models/member.model';
import { Router } from '@angular/router';
import { CooperativeModel } from './node_modules/src/app/shared/models/cooperative.model';
import { CooperativeService } from './node_modules/src/app/shared/services/cooperative.service';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss']
})
export class MemberListComponent implements OnInit {
  asEdit= false;
  form: any;
  cooperativeId = '';
  public isSubmit: boolean;
  
  closeResult = '';

  userId= '';
  memberListEmpty = true;

  _CoopId= '';
  cooperativeList: CooperativeModel[];
  memberList: MemberModel[] = [];
  dtTrigger= new Subject();
  memberDetails: MemberModel = {
    Id: '',
    IsActive: false,
    EffectiveDate: '',
    Firstname: '',
    Lastname: '',
    MemberCode: '',
    PhoneNumber: '',
    BirthDate: ''
  }
  dtExportButtonOptions: any = {};
  dtOptions: any;
  CoopSelected= false;
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    public member: MemberService,
    public router: Router,
    public cooperative: CooperativeService,

    ) {
      this.getCooperativeList();
    }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2,
      dom: 'Bfrtip',
      buttons: [
        'colvis',
        'copy',
        'print',
        'excel',
      ]
    };

    this.dtExportButtonOptions = {
      //ajax: json,
      columns: [{
        title: 'Code',
        data: 'MemberCode'
      }, {
        title: 'Nom',
        data: 'Lastname'
      }, {
        title: 'Prénoms',
        data: 'Firstname'
      }, {
        title: 'Date de naissance',
        data: 'BirthDate'
      }, 
      {
        title: 'Contact',
        data: 'PhoneNumber'
      }, 
      {
        title: 'Statut',
        data: 'IsActive'
      }, {
        title: 'Afficher',
        //data: 'salary'
      },
      {
        title: 'Modifier',
        //data: 'salary'
      },
      {
        title: 'Supprimer',
        //data: 'salary'
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    };
  }

  async getCooperativeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperativeList = res as CooperativeModel[];
      this.ngxService.stop();

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn(memberId) {
    sessionStorage.setItem("memberId",memberId);
    this.router.navigate(['/member/add'],{
    });
  }

  deleteTontineType(Id){

  }

  editTontineType(tontineType) {

  }

  updateTontineType(Id) {

  }

  createTontineType() {
    
  }
  

  async getMemberList(CoopId) {
    console.log('Get member: ', CoopId)
    this._CoopId = CoopId;
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.member.getMembersByCoopId(CoopId))
    .toPromise()
    .then(async (res) => {
      this.memberList = res as MemberModel[];
      await this.generateTable(JSON.stringify(this.memberList))
      //console.log('Memberss: ', this.memberList.length)
      console.log('Memberss: ', this.memberList)
      if ( !this.memberList ){
        this.CoopSelected = false;
        this.ngxService.stop();
        //this.extractData
        this.toastr.warning('Aucun membre dans la coopérative sélectionnée', 'Attention');

      }else {   
      this.CoopSelected = true;
      this.dtTrigger.next();
      this.ngxService.stop();
    }
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur détectée', err.error + ' ' + err);
      this.asEdit = false;
      this.CoopSelected = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async error() {
    this.toastr.error('Action impossible', 'Erreur');

  }

  confirmChangeMemberStatus(Id, status) {
    Swal.fire({
      title: 'Changement de statut',
      text: 'Voulez vous changer le statut du membre sélectionnée ?',
      type: 'warning',
      confirmButtonText: 'Changer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Changement de statut annulé', 'error');
        } else {
          Swal.fire('Succès', 'Statut en cours de changement', 'success');
          this.changeMemberStatus(Id,status)
        }
      });
  }

  confirmDeleteMember(Id) {
    Swal.fire({
      title: 'Confirmation de suppression',
      text: 'Voulez vous supprimer le membre sélectionnée',
      type: 'warning',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Suppression annulée', 'error');
        } else {
          await this.deleteMember(Id)
          Swal.fire('Succès',' membre en cours de suppression', 'success');

        }
      });
  }

  async deleteMember(Id) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.member.deleteMember(Id))
    .toPromise()
    .then((res) => {
      
      console.log('res delete Membre ', res);
      this.toastr.info('Suppression effectuée avec succès', 'Info');
      this.getMemberList(this._CoopId);
     if (this.memberList.length === 0) {
      this.memberListEmpty = true;
      this.toastr.error('Ipossible de  supprimer ce membre veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.memberListEmpty = false;
      this.toastr.success('Membre supprimé avec succès', 'Success');
      this.resetMemberTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.memberListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async resetMemberTable() {
    this.memberList =[{
      Id: '',
      IsActive: false,
      EffectiveDate: '',
      Firstname: '',
      Lastname: '',
      MemberCode: '',
      PhoneNumber: '',
      BirthDate: ''
        }];
   }

  
   async getMemberDetails(CoopId) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.member.getMemberById(CoopId))
      .toPromise()
      .then((res) => {
        console.log('Get coop details ', res);
        this.toastr.info('Chargement en cours', 'Info');
        this.memberDetails = res;
       if (!this.memberDetails) {
        this.toastr.error('Impossible d\'afficher ce membre veuillez réessayer', 'Erreur');
        this.modalService.dismissAll();
      } else {
        this.toastr.success('Données membre chargée avec succès', 'Success');
        
      }
       this.ngxService.stop();
      })
      .catch((err) => {
        this.memberListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
        console.log('An error occured ', err);
      });
  
     }

      async updateMember(MemberId, form) {
        this.ngxService.start();
        this.toastr.info('Chargement en cours', 'Info');
        (await this.member.updateMember(MemberId, form))
        .toPromise()
        .then((res) => {
          console.log('res create profile ', res);
          this.toastr.info('Modification en cours de traitement', 'Info');
          this.getMemberList(this._CoopId);
         if (this.memberList.length === 0) {
          this.memberListEmpty = true;
          this.toastr.error('Ipossible de modifier ce membre veuillez réessayer', 'Erreur');
          this.modalService.dismissAll();
        } else {
          this.memberListEmpty = false;
          this.toastr.success('Member modifié avec succès', 'Success');
          this.resetMemberTable();
          this.modalService.dismissAll();
        }
         this.ngxService.stop();
        })
        .catch((err) => {
          this.memberListEmpty = true;
          this.ngxService.stop();
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
          console.log('An error occured ', err);
        });

     }

     async changeMemberStatus(MemberId, status) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.member.changeMemberStatus(MemberId, status))
      .toPromise()
      .then((res) => {
        console.log('change user status profile ', res);
        this.toastr.info('Modification en cours de traitement', 'Info');
        this.getMemberList(this._CoopId);
       if (this.memberList.length === 0) {
        this.memberListEmpty = true;
        this.toastr.error('Ipossible de modifier le statut de ce membre  veuillez réessayer', 'Erreur');
        this.modalService.dismissAll();
      } else {
        this.memberListEmpty = false;
        this.toastr.success('Statut modifié avec succès', 'Success');
        this.resetMemberTable();
        this.modalService.dismissAll();
      }
       this.ngxService.stop();
      })
      .catch((err) => {
        this.memberListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
        console.log('An error occured ', err);
      });

   }
   async generateTable(json) {
     console.log('json: ', json);
    this.dtExportButtonOptions = {
      ajax: json,
      columns: [{
        title: 'Code',
        data: 'MemberCode'
      }, {
        title: 'Nom',
        data: 'Lastname'
      }, {
        title: 'Prénoms',
        data: 'Firstname'
      }, {
        title: 'Date de naissance',
        data: 'BirthDate'
      }, 
      {
        title: 'Contact',
        data: 'PhoneNumber'
      }, 
      {
        title: 'Statut',
        data: 'IsActive'
      }, {
        title: 'Afficher',
        //data: 'salary'
      },
      {
        title: 'Modifier',
        //data: 'salary'
      },
      {
        title: 'Supprimer',
        //data: 'salary'
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    };
   }

   /* private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  } */
  
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  
}


