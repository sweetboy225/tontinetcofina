import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {TontineParticipantComponent } from './tontine-participant.component';

describe('TontineParticipantComponent', () => {
  let component: TontineParticipantComponent;
  let fixture: ComponentFixture<TontineParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TontineParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TontineParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
