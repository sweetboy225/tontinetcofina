import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TontineParticipantRoutingModule } from './tontine-participant-routing.module';
import {TontineParticipantComponent } from './tontine-participant.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {CustomFormsModule} from 'ngx-custom-validators';
import {SelectModule} from 'ng-select';
import {DataTablesModule} from 'angular-datatables';
import {FormsModule} from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    TontineParticipantRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule,
    CustomFormsModule,
    SelectModule,
    DataTablesModule,
    FormsModule

  ],
  declarations: [TontineParticipantComponent],
  providers: [DatePipe]
})
export class TontineParticipantModule { }
