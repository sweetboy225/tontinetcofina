import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TontineParticipantComponent} from './tontine-participant.component';


const routes: Routes = [
  { 
    path: '',
    component: TontineParticipantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TontineParticipantRoutingModule { }
