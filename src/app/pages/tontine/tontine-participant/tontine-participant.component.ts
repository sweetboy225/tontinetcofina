import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Subject } from 'rxjs';

import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { TontineTypeModel } from 'src/app/shared/models/tontineType.model';
import { TontineService } from 'src/app/shared/services/tontine.service';
import { TontineModel } from 'src/app/shared/models/tontine.model';
import { Router } from '@angular/router';
import { CooperativeModel } from 'src/app/shared/models/cooperative.model';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';
import { MemberModel } from 'src/app/shared/models/member.model';
import { MemberService } from 'src/app/shared/services/member.service';

@Component({
  selector: 'app-tontine-participant',
  templateUrl: './tontine-participant.component.html',
  styleUrls: ['./tontine-participant.component.scss']
})
export class TontineParticipantComponent implements OnInit {
  asEdit= false;
  form: any;
  cooperativeId = '';
  tontineId= '';
  public isSubmit: boolean;
  tontineActivationDate: string;
  closeResult = '';
  _CoopId= '';
  part= 0;
  userId= '';
  tontineListEmpty = true;
  memberId= '';
  
  _TontineId= '';
  participantList: MemberModel[]= null;
  participantDetails: MemberModel = null;
  cooperativeList: CooperativeModel[];
  tontineTypeList: TontineTypeModel[];
  tontineList: TontineModel[] = [];
  dtTrigger= new Subject();
  tontineDetails: TontineModel = {
      Id: '',
      Name: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      Code: '',
      MembershipTicketFee: 0,
      Description: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      ContributionsAmount: 0,
      Frequency: {
        Value: 0,
        DisplayName: ''
      },
      Type: {
        Value: 0,
        DisplayName: ''
      },
      IsActive: true,
      EffectiveDate: '',
      BeginDate: '',
      EndDate: ''
  }
  enOn= false;
  dtExportButtonOptions: any = {};
  dtOptions: any;
  CoopSelected= false;
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    public tontine: TontineService,
    public router: Router,
    public cooperative: CooperativeService,
    public member: MemberService

    ) {
      this.getCooperativeList();
      this.getTontineTypeList();
      console.log('Date1: ', this.tontineActivationDate)
    }

    ngOnInit() {
      // this.resetParticipantTable();
    } 

  async getCooperativeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperativeList = res as CooperativeModel[];
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Une erreur a été détectée');
    });
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn(tontineId) {
    sessionStorage.setItem("tontineId",tontineId);
    this.router.navigate(['/tontine/add'],{
    });
  }

  deleteTontineType(Id){

  }

  editTontineType(tontineType) {

  }

  updateTontineType(Id) {

  }

  createTontineType() {
    
  }
  
  async getTontineTypeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getTontineType())
    .toPromise()
    .then(async (res) => {
      this.tontineTypeList = res as TontineTypeModel[];
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Erreur de connexion');
    });
  }
  
  async getTontineList(CoopId) {
    await this.resetParticipantTable();
    this._TontineId = CoopId;
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getTontinesByCooperative(CoopId))
    .toPromise()
    .then(async (res) => {
      //console.log('Get tontine list res: ', res.length, res.values)
      if ( res == null ){
        this.toastr.warning('Aucune tontine dans la coopérative sélectionnée', 'Attention');
        this.CoopSelected = false;
        this.ngxService.stop();
        //this.extractData()
      }else {   
      this.CoopSelected = true;
      //this.dtTrigger.next();
      this.ngxService.stop();
      res.forEach(element => {
        if(!element.Name[1].Text) {
          element.Name[1].Text= ''
        }
        if(!element.Description[1]) {
          element.Description.push({
            'CultureName': 'en-EN',
            'Text':''
          })
        } else if(!element.Description[1].Text) {
          element.Description[1].Text= ''
        }
        if(!element.EffectiveDate) {
          element.EffectiveDate= ''
        }
      });
      this.tontineList = res as TontineModel[];
    }
      
      //await this.generateTable(JSON.stringify(this.tontineList))
      //console.log('Tontiness: ', this.tontineList.length)

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur détectée', err.error + ' ' + err);
      this.asEdit = false;
      this.CoopSelected = false;
      this.toastr.warning('Aucune tontine trouvée pour cette coopérative', 'Cooperative vide');
    });
  }

  async getParticipantList(tontineId) {
    //await this.resetParticipantTable();
    this._TontineId = tontineId;
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getParticipantByTontine(tontineId))
    .toPromise()
    .then(async (res) => {
      console.log('Participant tontine list res: ', res)
      if ( res.length == 0 ){
        this.toastr.warning('Aucun participant dans la tontine sélectionnée', 'Attention');
        this.CoopSelected = false;
        this.ngxService.stop();
        //this.extractData()
      }else {   
      this.CoopSelected = true;
      //this.dtTrigger.next();
      this.ngxService.stop();
      this.participantList = res;
    }
      
      //await this.generateTable(JSON.stringify(this.tontineList))
      //console.log('Tontiness: ', this.tontineList.length)

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur détectée', err.error + ' ' + err);
      this.asEdit = false;
      this.CoopSelected = false;
      this.toastr.warning('Aucun participant trouvé pour cette tontinee', 'Tontine vide');
    });
  }

  async error() {
    this.toastr.error('Action impossible', 'Erreur');

  }

  confirmChangeTontineStatus(Id, status) {
    Swal.fire({
      title: 'Changement de statut',
      text: 'Voulez vous changer le statut de la tontine sélectionnée ?',
      type: 'warning',
      confirmButtonText: 'Changer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Changement de statut annulé', 'error');
        } else {
          Swal.fire({
            text: 'Date d\'activation:',
            html: '<h5 class="mt-5">Veuillez choisir la date d\'activation de la tontine SVP</h5> <hr><input  id="tontineActivationDate" name="tontineActivationDate" ngModel [(ngModel)]="tontineActivationDate" type="date" class="swal2-input">',
          }).then(async (result) => {
            console.log('Result: ',  $('#tontineActivationDate').val());
            this.changeTontineStatus(Id,status, $('#tontineActivationDate').val());
            new Promise(function (resolve) {
              resolve([
                $('#tontineActivationDate').val(),
                console.log('Result: ',  $('#tontineActivationDate').val()),
                //console.log('Date activation: ', $('#tontineActivationDate').val(), this.tontineActivationDate),
                //this.tontineActivationDate = $('#tontineActivationDate').val(),
              ])
            })
            if (result.value) {
              Swal.fire('Succès', 'Statut en cours de changement', 'success');
              console.log('Date activation: ', $('#tontineActivationDate').val(), this.tontineActivationDate)
            }
          });
         
        }
      });
  }

  confirmDeleteTontine(Id) {
    Swal.fire({
      title: 'Confirmation de suppression',
      text: 'Voulez vous supprimer la tontine sélectionnée',
      type: 'warning',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Suppression annulée', 'error');
        } else {
          await this.deleteTontine(Id)
          Swal.fire('Succès',' tontine en cours de suppression', 'success');

        }
      });
  }

  async deleteTontine(Id) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.tontine.deleteTontine(Id))
    .toPromise()
    .then((res) => {
      
      console.log('res delete Membre ', res);
      this.toastr.info('Suppression effectuée avec succès', 'Info');
      this.getTontineList(this._TontineId);
     if (this.tontineList.length === 0) {
      this.tontineListEmpty = true;
      this.toastr.error('Ipossible de  supprimer cette tontine veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.tontineListEmpty = false;
      this.toastr.success('Membre supprimé avec succès', 'Success');
      this.resetParticipantTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.tontineListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async resetParticipantTable() {
    /* this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 0,
      searching: false,
      dom: 'Bfrtip',
      buttons: [
        'colvis',
        'copy',
        'print',
        'excel',
      ]
    };

    this.dtExportButtonOptions = {
      //ajax: json,
      columns: [{
        title: 'Code',
        data: 'TontineCode'
      }, {
        title: 'Nom',
        data: 'Lastname'
      }, {
        title: 'Prénoms',
        data: 'Firstname'
      }, {
        title: 'Date de naissance',
        data: 'BirthDate'
      }, 
      {
        title: 'Contact',
        data: 'PhoneNumber'
      }, 
      {
        title: 'Statut',
        data: 'IsActive'
      }, {
        title: 'Afficher',
        //data: 'salary'
      },
      {
        title: 'Modifier',
        //data: 'salary'
      },
      {
        title: 'Supprimer',
        //data: 'salary'
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    }; */
    //this.tontineList= new TontineModel[0];
     this.tontineList =[{
      Id: '',
      Name: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      Code: '',
      MembershipTicketFee: 0,
      Description: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      ContributionsAmount: 0,
      Frequency: {
        Value: 0,
        DisplayName: ''
      },
      Type: {
        Value: 0,
        DisplayName: ''
      },
      IsActive: true,
      EffectiveDate: '',
      BeginDate: '',
      EndDate: ''
        }];
   }

  
   async getTontineDetails(CoopId) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.tontine.getTontineById(CoopId))
      .toPromise()
      .then((res) => {
        
        console.log('Get coop details ', res);
        this.toastr.info('Chargement en cours', 'Info');
        this.tontineDetails = res;
        if(!this.tontineDetails.Name[1].Text) {
          this.tontineDetails.Name[1].Text= ''
        }
        if(!this.tontineDetails.Description[1]) {
          this.tontineDetails.Description.push({
            'CultureName': 'en-EN',
            'Text':''
          })
        } else if(!this.tontineDetails.Description[1].Text) {
          this.tontineDetails.Description[1].Text= ''
        }
        if(!this.tontineDetails.EffectiveDate) {
          this.tontineDetails.EffectiveDate= ''
        }
       if (!this.tontineDetails) {
        this.toastr.error('Impossible d\'afficher cette tontine veuillez réessayer', 'Erreur');
        this.modalService.dismissAll();
      } else {
        this.toastr.success('Données de la tontine chargée avec succès', 'Success');
        
      }
       this.ngxService.stop();
      })
      .catch((err) => {
        this.tontineListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
        console.log('An error occured ', err);
      });
  
     }

     async getParticipantDetails(MemberId) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.member.getMemberById(MemberId))
      .toPromise()
      .then((res) => {  
        console.log('Get coop details ', res);
        this.toastr.info('Chargement en cours', 'Info');
        this.participantDetails = res;
        this.ngxService.stop();
      })
      .catch((err) => {
        this.tontineListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
        console.log('An error occured ', err);
      });
  
     }
     

      async updateTontine(TontineId, form) {
        this.ngxService.start();
        this.toastr.info('Chargement en cours', 'Info');
        (await this.tontine.updateTontine(TontineId, form))
        .toPromise()
        .then((res) => {
          console.log('res create profile ', res);
          this.toastr.info('Modification en cours de traitement', 'Info');
          this.getTontineList(this._TontineId);
         if (this.tontineList.length === 0) {
          this.tontineListEmpty = true;
          this.toastr.error('Ipossible de modifier cette tontine veuillez réessayer', 'Erreur');
          this.modalService.dismissAll();
        } else {
          this.tontineListEmpty = false;
          this.toastr.success('Tontine modifié avec succès', 'Success');
          this.resetParticipantTable();
          this.modalService.dismissAll();
        }
         this.ngxService.stop();
        })
        .catch((err) => {
          this.tontineListEmpty = true;
          this.ngxService.stop();
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
          console.log('An error occured ', err);
        });

     }

     async getMemberList(CoopId) {
      console.log('Get member: ', CoopId)
      this._CoopId = CoopId;
      this.ngxService.start();
      this.toastr.info('Traitement en cours', 'Info');
      (await this.member.getMembersByCoopId(CoopId))
      .toPromise()
      .then(async (res) => {
        this.participantList = res as MemberModel[];
        await this.generateTable(JSON.stringify(this.participantList))
        //console.log('Memberss: ', this.participantList.length)
        console.log('Memberss: ', this.participantList)
        if ( this.participantList.length == 0 ){
          this.CoopSelected = true;
          this.ngxService.stop();
          this.toastr.warning('Aucun membre dans la coopérative sélectionnée', 'Attention');
        }else {   
        this.CoopSelected = false;
        this.dtTrigger.next();
        this.ngxService.stop();

      }
      })
      .catch((err) => {
        this.ngxService.stop();
        console.warn('une erreur détectée', err.error + ' ' + err);
        this.asEdit = false;
        this.CoopSelected = false;
        this.toastr.error('Veuillez réessayer', 'Authentification impossible');
      });
    }

     async changeTontineStatus(TontineId, status, date) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.tontine.changeTontineStatus(TontineId, status, date))
      .toPromise()
      .then((res) => {
        if(res == true) {
          this.toastr.success('Statut changé avec succès ', 'Succès');
          this.getTontineList(this._TontineId);  
        } else {
          this.toastr.error('Ipossible de modifier le statut de cette tontine  veuillez réessayer', 'Erreur');
          this.modalService.dismissAll();  
          this.getTontineList(this._TontineId);  
        }
       
       this.ngxService.stop();
      })
      .catch((err) => {
        this.tontineListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        console.log('An error occured ', err);
      });

   }
   async generateTable(json) {
     console.log('json: ', json);
    this.dtExportButtonOptions = {
      ajax: json,
      columns: [{
        title: 'Code',
        data: 'TontineCode'
      }, {
        title: 'Nom',
        data: 'Lastname'
      }, {
        title: 'Prénoms',
        data: 'Firstname'
      }, {
        title: 'Date de naissance',
        data: 'BirthDate'
      }, 
      {
        title: 'Contact',
        data: 'PhoneNumber'
      }, 
      {
        title: 'Statut',
        data: 'IsActive'
      }, {
        title: 'Afficher',
        //data: 'salary'
      },
      {
        title: 'Modifier',
        //data: 'salary'
      },
      {
        title: 'Supprimer',
        //data: 'salary'
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    };
   }

   /* private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  } */
  
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  langueBtn() {
    this.enOn = !this.enOn;
  }
  
  async addParticipants2Tontine() {
    this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await (this.tontine.addParticipants2Tontine(this.tontineId, this.participantDetails.Id, this.part)))
      .toPromise()
      .then((res) => {
        if(res == true) {
          this.toastr.success('Participant ajouté avec succès ', 'Succès');
          this.getTontineList(this._TontineId);  
        } else {
          this.toastr.error('Ipossible d\'ajouter ce participant,  veuillez réessayer', 'Erreur');
          this.modalService.dismissAll();  
          this.getTontineList(this._TontineId);  
        }
       
       this.ngxService.stop();
      })
      .catch((err) => {
        this.tontineListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        console.log('An error occured ', err);
      });
  }
}


