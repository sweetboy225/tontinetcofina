import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TontineRoutingModule } from './tontine-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TontineRoutingModule
  ]
})
export class TontineModule { }
 