import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SettingsAppRoutingModule } from './settings-app-routing.module';
import {SettingsAppComponent } from './settings-app.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
@NgModule({
  imports: [
    CommonModule,
   SettingsAppRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule
  ],
  declarations: [SettingsAppComponent],
  providers: [DatePipe]
})
export class SettingsAppModule { }
