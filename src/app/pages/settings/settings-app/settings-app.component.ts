import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap'; 


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-settings-app',
  templateUrl: './settings-app.component.html',
  styleUrls: ['./settings-app.component.scss']
})
export class SettingsAppComponent implements OnInit {
  asEdit= false;
  editUser= false;
  closeResult = '';
  newRoleId = '';
  UserDetails: SecurityModel = {
    Id: '',
    SecurityStamp: '',
    FirstName: '',
    LastName: '',
    PhoneNumber: '',
    Email: '',
    Username: '',
    ClientId: '',
    GroupId: '',
    CenterId: 0,
    Language:0,
    Roles: RoleModel[0],
    
    PermissionSets: PermissionSetsModel[0]
};

  newUser: SecurityModel = {
      Id: '',
      SecurityStamp: '',
      FirstName: '',
      LastName: '',
      PhoneNumber: '',
      Email: '',
      Username: '',
      ClientId: '',
      GroupId: '',
      CenterId: 0,
      Language:0,
      Roles: RoleModel[0],
      PermissionSets: PermissionSetsModel[0]
  };

  usersList: SecurityModel[] = [{
    Id: '',
    SecurityStamp: '',
    FirstName: '',
    LastName: '',
    PhoneNumber: '',
    Email: '',
    Username: '',
    ClientId: '',
    GroupId: '',
    CenterId: 0,
    Language:0,
    Roles: RoleModel[0],
    PermissionSets: PermissionSetsModel[0]
  }];


  newProfile: RoleModel = {
    Id: '',
    Name: ''
  };

  usersProfilesList: RoleModel[] = [{
    Id: '',
    Name: ''
  }];
  usersListEmpty = true;
  usersProfilesListEmpty = true;
  callerNameListEmpty = true;

  newRole: '';
  newUsername: '';
  newLastName: '';
  newFirstName: '';
  newEmail: '';
  newCenterId: 0;

  todayDate: Date;
  newDate: string;
  serviceId: string;
  townID: string;
  centerId = 0;
  date: Date;

  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private security: SecurityService
    ) {
      this.refreshUsersProfilesList();
      this.refreshUsersList();
    }

  ngOnInit() {

  }
  editBtn(user) {
  this.newUser = user;
  }

  editRoleBtn(role) {
    this.newProfile = role;
  }

  async refreshUsersList() {
    console.log('Refresh users')
    this.usersListEmpty = true;
    await this.resetUsersTable();
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.getAllUsers())
    .toPromise()
    .then((res) => {
     this.usersList = res as SecurityModel[]; 
     console.log('list', this.usersList)
     if ( this.usersList.length === 0) {
      this.usersListEmpty = true;
      this.resetUsersTable();
      this.toastr.error('Utilisateurs introuvable veuillez réessayer', 'Erreur');
    } else {
      this.usersListEmpty = false;
      this.toastr.success('Utilisateurs chargé avec succès', 'Success');
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
  }


  async refreshUsersProfilesList() {
    this.usersProfilesListEmpty = true;
    await this.resetUsersProfilesTable();
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.getRolesList())
    .toPromise()
    .then((res) => {
      console.log('res profile');
     this.usersProfilesList = res as RoleModel[];
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.resetUsersTable();
      this.toastr.error('Profil utilisateurs introuvable veuillez réessayer', 'Erreur');
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Utilisateurs chargé avec succès', 'Success');
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

  resetUsersTable() {
    this.usersList = [{
      Id: '',
      SecurityStamp: '',
      FirstName: '',
      LastName: '',
      PhoneNumber: '',
      Email: '',
      Username: '',
      ClientId: '',
      GroupId: '',
      CenterId: 0,
      Language:0,
      Roles: RoleModel[0],
      PermissionSets: PermissionSetsModel[0]
      }];
  }

  resetUsersProfilesTable() {
    this.usersProfilesList = [{
      Id: '',
      Name: '',
    }];
  }

  resetCallerNameTable() {
    this.usersProfilesList = [{
      Id: '',
      Name: '',
    }];
  }
  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  async createRole() {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.createRole(this.newProfile.Name))
    .toPromise()
    .then(async (res) => {
      console.log('res create profile ', res);
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.resetUsersProfilesTable();
      await this.refreshUsersProfilesList();
      this.toastr.error('Ipossible de créer ce profil veuillez réessayer', 'Erreur');
    } else {
      await this.refreshUsersProfilesList();
      this.usersProfilesListEmpty = false;
      this.toastr.success('Profil créé avec succès', 'Success');
    }
     this.ngxService.stop();
     this.modalService.dismissAll();
    })
    .catch(async (err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.modalService.dismissAll();
      this.resetUsersProfilesTable();
      await this.refreshUsersProfilesList();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async createUser() {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.registerUser(this.newFirstName))
    .toPromise()
    .then((res) => {
      console.log('res create profile ', res);
      // this.toastr.info(res as string, 'Info');
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.resetUsersTable();
      this.toastr.error('Ipossible de créer ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Profil créé avec succès', 'Success');
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async updateUser(UserId, form) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.updateUser(UserId, form))
    .toPromise()
    .then((res) => {
      console.log('res create profile ', res);
      this.toastr.info('Modification effectuée avec succès', 'Info');
      this.refreshUsersList();
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.toastr.error('Ipossible de modifier ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Profil modifié avec succès', 'Success');
      this.resetUsersTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async updaterole() {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.updateRole(this.newProfile.Id, this.newProfile.Name ))
    .toPromise()
    .then((res) => {
      console.log('res edit profile ', res);
      this.toastr.info('Modification effectuée avec succès', 'Info');
      this.refreshUsersList();
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.toastr.error('Ipossible de modifier ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Profil modifié avec succès', 'Success');
      this.resetUsersTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    }); 
   }
   async confirmDelete(Text, Id) {
    Swal.fire({
      title: 'Confirmation de suppression',
      text: 'Voulez vous supprimer l\'élément sélectionné',
      type: 'warning',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('', 'Suppression annulée', 'error');
        } else {
          Swal.fire('', Text + ' supprimé avec succès', 'success');
          if(Text == 'Profil utilisateur') {
            await this.deleteRole(Id);
          } else{
            await this.deleteUser(Id);
          }

        }
      });
  }
  
   async deleteRole(RoleId) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.DeleteRole(RoleId))
    .toPromise()
    .then((res) => {
      console.log('res delete profile ', res);
      this.toastr.info('Suppression effectuée avec succès', 'Info');
      this.refreshUsersProfilesList();
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.toastr.error('Ipossible de  supprimer ce profile veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Role supprimé avec succès', 'Success');
      this.resetUsersTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async deleteUser(UserId) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.DeleteUser(UserId))
    .toPromise()
    .then((res) => {
      console.log('res delete profile ', res);
      this.toastr.info('Suppression effectuée avec succès', 'Info');
      this.refreshUsersList();
     if (this.usersProfilesList.length === 0) {
      this.usersProfilesListEmpty = true;
      this.toastr.error('Ipossible de  supprimer cet utilisateur veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Utilisateur modifié avec succès', 'Success');
      this.resetUsersTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }
   async getUserDetails(UserId) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.getUserById(UserId))
    .toPromise()
    .then((res) => {
      console.log('Get user details ', res);
      this.toastr.info('Chargement en cours', 'Info');
      this.UserDetails = res;
     if (!this.UserDetails) {
      this.toastr.error('Ipossible de modifier ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.toastr.success('Données utilisateur chargée avec succès', 'Success');
      
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });

   }

   async addRole2User(UserId) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.addRole2User(this.newRoleId, UserId))
    .toPromise()
    .then((res) => {
      console.log('Add role to user ', res);
      this.toastr.info('Chargement en cours', 'Info');
      //this.UserDetails = res;
     if (!this.UserDetails) {
      this.toastr.error('Ipossible de modifier ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.toastr.success('Données utilisateur chargée avec succès', 'Success');
      this.editUser= false;
      this.getUserDetails(UserId)
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });

   }
   editUserRole(){
     this.editUser= true
   }

   async deleteRoleUser(RoleId, UserId) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.security.DeleteRoleUser(RoleId, UserId))
    .toPromise()
    .then((res) => {

     if (res === false) {
      this.toastr.error('Ipossible de  supprimer ce profile veuillez réessayer', 'Erreur');
      //this.modalService.dismissAll();
      this.getUserDetails(UserId);

    } else {
      this.usersProfilesListEmpty = false;
      this.toastr.success('Role supprimé avec succès', 'Success');
      this.getUserDetails(UserId);
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
   }
}

