import {SettingsAppModule } from './settings-app.module'

describe('GestionUtilisateurAfficherModule', () => {
  let basicButtonModule: SettingsAppModule;

  beforeEach(() => {
    basicButtonModule = new SettingsAppModule();
  });

  it('should create an instance', () => {
    expect(basicButtonModule).toBeTruthy();
  });
});
