import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {SettingsTontineComponent } from './settings-tontine.component';

describe('SettingsTontineComponent', () => {
  let component: SettingsTontineComponent;
  let fixture: ComponentFixture<SettingsTontineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingsTontineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsTontineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
