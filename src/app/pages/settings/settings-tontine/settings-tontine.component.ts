import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { TontineTypeModel } from 'src/app/shared/models/tontineType.model';
import { TontineService } from 'src/app/shared/services/tontine.service';

@Component({
  selector: 'app-settings-tontine',
  templateUrl: './settings-tontine.component.html',
  styleUrls: ['./settings-tontine.component.scss']
})
export class SettingsTontineComponent implements OnInit {
  asEdit= false;
  form: any;

  public isSubmit: boolean;

  closeResult = '';
  newTontineType: TontineTypeModel = {
      Id: '',
      Name: ''
  };

  tontineTypeList: TontineTypeModel[]
  userId= '';
  usersListEmpty = true;

  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    public tontine: TontineService
    ) {
      this.getTontineTypeList()
    }

  ngOnInit() {
    this.tontineTypeList = [{
      Id: '',
      Name: ''
    }];
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = true;
  }

  async confirmDelete(Id) {
    Swal.fire({
      title: 'Confirmation de suppression',
      text: 'Voulez vous supprimer l\'élément sélectionné',
      type: 'warning',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('', 'Suppression annulée', 'error');
        } else {
          Swal.fire('', Text + ' supprimé avec succès', 'success');
            await this.deleteTontineType(Id);

        }
      });
  }

  deleteTontineType(Id){

  }

  editTontineType(tontineType) {

  }

  updateTontineType(Id) {

  }

  createTontineType() {
    
  }

  async getTontineTypeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.tontine.getTontineType())
    .toPromise()
    .then(async (res) => {
      this.tontineTypeList = res as TontineTypeModel[];
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async error() {
    this.toastr.error('Action impossible', 'Erreur');

  }
  
}


