import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SettingsTontineComponent} from './settings-tontine.component';


const routes: Routes = [
  {
    path: '',
    component: SettingsTontineComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsTontineRoutingModule { }
