import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-settings-user',
  templateUrl: './settings-user.component.html',
  styleUrls: ['./settings-user.component.scss']
})
export class SettingsUserComponent implements OnInit {
  asEdit= false;
  form: any;

  public isSubmit: boolean;

  closeResult = '';
  UserDetails: SecurityModel = {
      Id: '',
      SecurityStamp: '',
      FirstName: '',
      LastName: '',
      PhoneNumber: '',
      Email: '',
      Username: '',
      ClientId: '',
      GroupId: '',
      CenterId: 0,
      Language:0,
      Roles: RoleModel[0],
      PermissionSets: PermissionSetsModel[0]
  };
  userId= '';
  usersListEmpty = true;

  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private security: SecurityService
    ) {
      this.userId = localStorage.getItem('userId');
      console.log('0', this.userId);  
      this.getUserDetails();
      this.isSubmit = false;
    }

  ngOnInit() {
    this.resetUserDetails();
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = true;
  }

  async updateUserDetails(form) {
    console.log('Start editing');
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.updateUser(this.userId, form))
    .toPromise()
    .then(async (res) => {
      console.log('Modification: ', res)
      this.toastr.success('modification réussie veuillez patienter', 'Success');
      this.ngxService.stop();
      await this.resetUserDetails();
      await this.getUserDetails();
      this.asEdit = false;
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez vérifier les données saisies', 'Authentification impossible');
    });
  }
  

   async getUserDetails() {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    console.log('UserId1:', this.userId );
    (await this.security.getUserById(this.userId))
    .toPromise()
    .then((res) => {
      console.log('Get user details ', res);
      this.toastr.info('Données utilisateur chargée avec succès', 'Info');
      this.UserDetails = res;
     if (!this.UserDetails) {
      this.toastr.error('Ipossible de modifier ce profil veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.toastr.success('Profil modifié avec succès', 'Success');
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.usersListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });

   }

   async resetUserDetails() {
    this.UserDetails = {
      Id: '',
      SecurityStamp: '',
      FirstName: '',
      LastName: '',
      PhoneNumber: '',
      Email: '',
      Username: '',
      ClientId: '',
      GroupId: '',
      CenterId: 0,
      Language:0,
      Roles: RoleModel[0],
      PermissionSets: PermissionSetsModel[0]
    };
   }

   async resetPassword() {
     const body= {"Email": this.UserDetails.Email}
     console.log('forms', body)
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.forgotPassword(body))
    .toPromise()
    .then((res) => {
      this.ngxService.stop();
      this.toastr.success(res, 'Email envoyé');
      this.asEdit = false;
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn(err);
      if (err.status == 400) {
      this.toastr.error('Veuillez vérifier les données saisies', 'Erreur');
      } else {
      this.toastr.error(err.error, 'Erreur');
    }
    });
  }
}

