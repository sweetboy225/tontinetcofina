import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SettingsUserRoutingModule } from './settings-user-routing.module';
import {SettingsUserComponent } from './settings-user.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {CustomFormsModule} from 'ngx-custom-validators';

@NgModule({
  imports: [
    CommonModule,
   SettingsUserRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule,
    CustomFormsModule

  ],
  declarations: [SettingsUserComponent],
  providers: [DatePipe]
})
export class SettingsUserModule { } 
