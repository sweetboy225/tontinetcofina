import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'app',
        loadChildren: () => import('./settings-app/settings-app.module').then(m => m.SettingsAppModule)
      },
      {
        path: 'user',
        loadChildren: () => import('./settings-user/settings-user.module').then(m => m.SettingsUserModule)
      },
      {
        path: 'tontine',
        loadChildren: () => import('./settings-tontine/settings-tontine.module').then(m => m.SettingsTontineModule)
      },
      {
        path: 'country',
        loadChildren: () => import('./settings-country/settings-country.module').then(m => m.SettingsCountryModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
  