import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap'; 


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { GeneralService } from 'src/app/shared/services/general.service';
import { data } from 'src/app/shared/models/data.model';

@Component({
  selector: 'app-settings-country',
  templateUrl: './settings-country.component.html',
  styleUrls: ['./settings-country.component.scss']
})
export class SettingsCountryComponent implements OnInit {
  countryListEmpty= true;
  asEdit= false;
  editUser= false;
  newTown= TownModel;
  closeResult = '';
  newRoleId = '';
  CountryDetails: CountryModel = {
    Id: '',
    Code: '',
    Name: data[0], 
    Description: data[0]
};

  newCountry: CountryModel = {
    Id: '',
    Code: '',
    Name: data[0], 
    Description: data[0]
  };

  countryList: CountryModel[] = [{
    Id: '',
    Code: '',
    Name: data[0], 
    Description: data[0]
  }];

  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private general: GeneralService
    ) {
      console.log('Controller');
      this.refreshCountryList();
    }

  ngOnInit() {

  }
  editBtn(country) {
  this.newCountry = country;
  }

  editRoleBtn(town) {
    this.newTown= town;
  }

  async refreshCountryList() {
    console.log('Test');
    this.countryListEmpty = true;
    //await this.resetCountriesTable();
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.general.getCountryList())
    .toPromise()
    .then((res) => {
     this.countryList = res as CountryModel[]; 
     console.log('list', this.countryList)
     console.log('length', this.countryList.length);
     console.log('Langue', this.countryList[0].Name);
     if ( this.countryList.length === 0) {
      console.log('Test1');
      this.countryListEmpty = true;
      //this.resetCountriesTable();
      this.toastr.error('Utilisateurs introuvable veuillez réessayer', 'Erreur');
    } else { 
      console.log('Test2');
      this.countryListEmpty = false;
      this.toastr.success('Utilisateurs chargé avec succès', 'Success');
    }
     this.ngxService.stop();
     console.log('Test3');
    })
    .catch((err) => {
      this.countryListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
  }

  async getCountryDetails(countryId) {
    
  }

  open(content) {

  }
}

