import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SettingsCountryRoutingModule } from './settings-country-routing.module';
import {SettingsCountryComponent } from './settings-country.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
@NgModule({
  imports: [
    CommonModule,
   SettingsCountryRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule
  ],
  declarations: [SettingsCountryComponent],
  providers: [DatePipe]
})
export class SettingsCountryModule { }
