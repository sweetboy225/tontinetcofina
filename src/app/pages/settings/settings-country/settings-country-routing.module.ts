import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SettingsCountryComponent} from './settings-country.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsCountryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsCountryRoutingModule { }
