import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { GeneralService } from 'src/app/shared/services/general.service';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { TontineService } from 'src/app/shared/services/tontine.service';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';

import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cooperative-add',
  templateUrl: './cooperative-add.component.html',
  styleUrls: ['./cooperative-add.component.scss']
})
export class CooperativeAddComponent implements OnInit {
  asEdit= false;
  form: any;

  public isSubmit: boolean;
 
  closeResult = '';
  countries: CountryModel[];
  towns: TownModel[];
  userId= '';
  usersListEmpty = true;
  CultureNameEn= 'en-US';
  CultureNameFr= 'fr-FR';
  cooperativeId= '';
  enOn= false;
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private general: GeneralService,
    private cooperative: CooperativeService,
    private router: Router,
    private activateRouter:ActivatedRoute
    ) {
      this.userId = localStorage.getItem('userId');
      this.getCountriesList();
      this.isSubmit = false;
    }

  ngOnInit() {
    this.cooperativeId = sessionStorage.getItem('cooperativeId')
    console.log('Coop Id: ', this.cooperativeId ) 
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = true;
  }

  langueBtn() {
    this.enOn = !this.enOn;
  }

  async getTownById(Id) {
    
  }

  async getCountriesList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getCountryList())
    .toPromise()
    .then(async (res) => {
      this.countries = res as CountryModel[];
      console.log('Country: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }
   
  async getTown(country) {
    console.log(country.target.value);
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getTownsList(country.target.value))
    .toPromise()
    .then(async (res) => {
      this.towns = res as TownModel[];
      console.log('Town: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async createCooperative(cooperativeForm) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.createCooperative(cooperativeForm))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Coopérative crée veuillez patienter', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/cooperative/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Ajout impossible');
    });

  }
  async resetForm() {

  }
}

