import {CooperativeAddModule } from './cooperative-add.module';

describe('GestionUtilisateurAfficherModule', () => {
  let basicButtonModule: CooperativeAddModule;

  beforeEach(() => {
    basicButtonModule = new CooperativeAddModule();
  });

  it('should create an instance', () => {
    expect(basicButtonModule).toBeTruthy();
  });
});
 