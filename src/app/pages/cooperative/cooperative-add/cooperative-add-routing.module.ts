import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CooperativeAddComponent} from './cooperative-add.component';

const routes: Routes = [
  {
    path: '',
    component: CooperativeAddComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CooperativeAddRoutingModule { }
