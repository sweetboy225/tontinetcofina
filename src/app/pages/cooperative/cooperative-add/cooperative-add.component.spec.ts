import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {CooperativeAddComponent } from './cooperative-add.component';

describe('CooperativeAddComponent', () => {
  let component: CooperativeAddComponent;
  let fixture: ComponentFixture<CooperativeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CooperativeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperativeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
