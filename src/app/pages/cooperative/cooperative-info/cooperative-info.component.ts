import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { GeneralService } from 'src/app/shared/services/general.service';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { TontineService } from 'src/app/shared/services/tontine.service';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';

import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { CooperativeModel } from 'src/app/shared/models/cooperative.model';

@Component({
  selector: 'app-cooperative-info',
  templateUrl: './cooperative-info.component.html',
  styleUrls: ['./cooperative-info.component.scss']
})
export class CooperativeInfoComponent implements OnInit {
  asEdit= false; 
  form: any;

  public isSubmit: boolean;
 
  closeResult = '';
  countries: CountryModel[];
  towns: TownModel[];
  userId= '';
  usersListEmpty = true;
  CultureNameEn= 'en-US';
  CultureNameFr= 'fr-FR';
  cooperativeId= '';
  enOn= false;
  cooperativeList: CooperativeModel[];
  cooperativeDetails: any = {
    Id: '',
    IsActive: true,
    EffectiveDate: '',
    TownId: '',
    Address: {
      City: '',
      CivicAddress: ''
    },
    PhoneNumbers: [
      {
        LineNumber: '',
        Extension: '',
        PhoneNumberType: 1
      }
    ],
    ContactPerson: {
      EmailAddress: '',
      FirstName: '',
      LastName: '',
      JobTitle: [
        {
          CultureName: 'fr-FR',
          Text: ''
        },
        {
          CultureName: 'en-EN',
          Text: ''
        }
      ],
      PhoneNumbers: [
        {
          LineNumber: '',
          Extension: '',
          PhoneNumberType: 1
        }
      ]
    },
    Code: '',
    Name: [
      {
        CultureName: '',
        Text: ''
      },
      {
        CultureName: '', 
        Text: ''
      }
    ],
    Description: [
      {
        CultureName: '',
        Text: ''
      },
      {
        CultureName: '',
        Text: ''
      }
    ]
  }
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private general: GeneralService,
    private cooperative: CooperativeService,
    private router: Router,
    private activateRouter:ActivatedRoute
    ) {
      this.getCooperativeList();
      this.userId = localStorage.getItem('userId');
      this.getCountriesList();
      this.isSubmit = false;
      this.cooperativeId = sessionStorage.getItem("viewedCooperativeId");
      console.log(this.cooperativeId)
      if(sessionStorage.getItem("viewedCooperativeId")){
        this.cooperativeId = sessionStorage.getItem('viewedCooperativeId')
        this.getCoopDetails(this.cooperativeId);
        console.log('Test 1: ', this.cooperativeId)
      } else {
        this.cooperativeId = ''
        console.log('Test 2: ', this.cooperativeId)
      }
    }

  ngOnInit() {
    console.log('Coop Id: ', this.cooperativeId ) 
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = !this.asEdit;
    console.log('Modification en cours: ', this.asEdit)
  }

  langueBtn() {
    this.enOn = !this.enOn;
  }

  async getTownById(Id) {
    
  }

  async getCooperativeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperativeList = res as CooperativeModel[];
      this.ngxService.stop();

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }
  async getCountriesList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getCountryList())
    .toPromise()
    .then(async (res) => {
      this.countries = res as CountryModel[];
      console.log('Country: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }
   
  async getTown(country) {
    console.log(country.target.value);
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getTownsList(country.target.value))
    .toPromise()
    .then(async (res) => {
      this.towns = res as TownModel[];
      console.log('Town: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async createCooperative(cooperativeForm) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.createCooperative(cooperativeForm))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Coopérative crée veuillez patienter', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/cooperative/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }
  async resetForm() {

  }

  async getCoopDetails(CoopId) {
    console.log('La Coop: ', CoopId)
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.cooperative.getCooperativeById(CoopId))
    .toPromise()
    .then((res) => {
      console.log('Get coop details ', res);
      this.toastr.info('Chargement en cours', 'Info');
      this.cooperativeDetails = res;
      if(!this.cooperativeDetails.EffectiveDate) {
        this.cooperativeDetails.EffectiveDate = 'Unactive'          
      }
      if(this.cooperativeDetails.Name[0]) {
        this.cooperativeDetails.Name[0] = {
          CultureName: this.cooperativeDetails.Name[0].CultureName + '',
          Text: this.cooperativeDetails.Name[0].Text + '',
        }
      } else {
        this.cooperativeDetails.Name[0] = {
          CultureName: ' ',
          Text: ' ',
        }
      }
      if(this.cooperativeDetails.Name[1]) {
        this.cooperativeDetails.Name[1] = {
          CultureName: this.cooperativeDetails.Name[1].CultureName + '',
          Text: this.cooperativeDetails.Name[1].Text + '',
        }
      } else {
        this.cooperativeDetails.Name[1] = {
          CultureName: ' ',
          Text: ' ',
        }
      }

      if(this.cooperativeDetails.Description[0]) {
        this.cooperativeDetails.Description[0] = {
          CultureName: this.cooperativeDetails.Description[0].CultureName + '',
          Text: this.cooperativeDetails.Description[0].Text + '',
        }
      } else {
        this.cooperativeDetails.Description[0] = {
          CultureName: ' ',
          Text: ' ',
        }
      }
      if(this.cooperativeDetails.Description[1]) {
        this.cooperativeDetails.Description[1] = {
          CultureName: this.cooperativeDetails.Description[1].CultureName + '',
          Text: this.cooperativeDetails.Description[1].Text + '',
        }
      } else {
        this.cooperativeDetails.Description[1] = {
          CultureName: ' ',
          Text: ' ',
        }
      }
      if(!this.cooperativeDetails.Name[1].Text) {
        this.cooperativeDetails.Name[1].Text= ''
      }
      if(!this.cooperativeDetails.Description[1]) {
        this.cooperativeDetails.Description.push({
          'CultureName': 'en-EN',
          'Text':''
        })
      } else if(!this.cooperativeDetails.Description[1].Text) {
        this.cooperativeDetails.Description[1].Text= ''
      }
      if(!this.cooperativeDetails.EffectiveDate) {
        this.cooperativeDetails.EffectiveDate= ''
      }
     if (!this.cooperativeDetails) {
      this.toastr.error('Ipossible de modifier cette coopérative veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.toastr.success('Données coopérative chargée avec succès', 'Success');
      
    }
    console.log('Get coop details2 ', this.cooperativeDetails);

     this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });
    this.ngxService.stop();

   }
   ngOnDestroy() { 
    console.log(sessionStorage.getItem('viewedCooperativeId'))
     console.log('Coop info destroyed');
     sessionStorage.setItem("viewedCooperativeId",'');
     console.log(sessionStorage.getItem('viewedCooperativeId'))
     }

     async updateCoop(CoopId, form) {
      this.asEdit= false; 
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.cooperative.updateCooperative(CoopId, form))
      .toPromise()
      .then((res) => {
        console.log('res Update coop ', res);
        this.toastr.info('Modification en cours de traitement', 'Info');
        this.getCooperativeList();
       if (this.cooperativeList.length === 0) {
        this.toastr.error('Ipossible de modifier cette coopérative veuillez réessayer', 'Erreur');
      } else {
        this.getCoopDetails(CoopId)
        this.toastr.success('Cooperative modifié avec succès', 'Success');
      }
       this.ngxService.stop();
      })
      .catch((err) => {
        this.getCoopDetails(CoopId)
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        console.log('An error occured ', err);
      });

   }
}

