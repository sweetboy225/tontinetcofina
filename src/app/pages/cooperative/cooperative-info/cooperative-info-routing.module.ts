import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CooperativeInfoComponent} from './cooperative-info.component';

const routes: Routes = [
  {
    path: '',
    component: CooperativeInfoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CooperativeInfoRoutingModule { }
