import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {CooperativeInfoComponent } from './cooperative-info.component';

describe('CooperativeInfoComponent', () => {
  let component: CooperativeInfoComponent;
  let fixture: ComponentFixture<CooperativeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CooperativeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperativeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
