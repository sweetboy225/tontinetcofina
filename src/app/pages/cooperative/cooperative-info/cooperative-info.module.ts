import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {CooperativeInfoRoutingModule } from './cooperative-info-routing.module';
import {CooperativeInfoComponent } from './cooperative-info.component';
import {SharedModule} from '../../../theme/shared/shared.module';
import {NgbButtonsModule, NgbDropdownModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import {CustomFormsModule} from 'ngx-custom-validators';

@NgModule({
  imports: [
    CommonModule,
   CooperativeInfoRoutingModule,
    SharedModule,
    NgbDropdownModule,
    NgxUiLoaderModule,
    NgbTabsetModule,
    CustomFormsModule

  ],
  declarations: [CooperativeInfoComponent],
  providers: [DatePipe]
})
export class CooperativeInfoModule { } 
