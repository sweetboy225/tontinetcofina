import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CooperativeRoutingModule } from './cooperative-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CooperativeRoutingModule
  ]
})
export class CooperativeModule { }
 