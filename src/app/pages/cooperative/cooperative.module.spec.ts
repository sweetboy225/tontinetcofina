import { CooperativeModule } from './cooperative.module';

describe('CooperativeModule', () => {
  let uiBasicModule: CooperativeModule;

  beforeEach(() => {
    uiBasicModule = new CooperativeModule();
  });

  it('should create an instance', () => {
    expect(uiBasicModule).toBeTruthy();
  });
});
