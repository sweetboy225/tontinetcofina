import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        loadChildren: () => import('./cooperative-add/cooperative-add.module').then(m => m.CooperativeAddModule)
      },
      {
        path: 'list',
        loadChildren: () => import('./cooperative-list/cooperative-list.module').then(m => m.CooperativeListModule)
      } ,
      {
        path: 'info',
        loadChildren: () => import('./cooperative-info/cooperative-info.module').then(m => m.CooperativeInfoModule)
      },
      /*{
        path: 'details',
        loadChildren: () => import('./cooperative-details/cooperative-details.module').then(m => m.CooperativeDetailsModule)
      }*/
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CooperativeRoutingModule { }
  