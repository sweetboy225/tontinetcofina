import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { TontineTypeModel } from 'src/app/shared/models/tontineType.model';
import { TontineService } from 'src/app/shared/services/tontine.service';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';
import { CooperativeModel } from 'src/app/shared/models/cooperative.model';
import { Router } from '@angular/router';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { GeneralService } from 'src/app/shared/services/general.service';

@Component({  
  selector: 'app-cooperative-list',
  templateUrl: './cooperative-list.component.html',
  styleUrls: ['./cooperative-list.component.scss']
})
export class CooperativeListComponent implements OnInit {
  asEdit= false;
  form: any;
  _date: Date = new Date;
  public isSubmit: boolean;
  countries: CountryModel[];

  towns: TownModel[];

  enOn= false;

  closeResult = ''; 
  newTontineType: TontineTypeModel = {
      Id: '',
      Name: ''
  };
 
  tontineTypeList: TontineTypeModel[];
  userId= '';
  cooperativeListEmpty = true;

  cooperativeList: CooperativeModel[];
  cooperativeDetails: any = {
    Id: '',
    IsActive: true,
    EffectiveDate: '',
    TownId: '',
    Address: {
      City: '',
      CivicAddress: ''
    },
    PhoneNumbers: [
      {
        LineNumber: '',
        Extension: '',
        PhoneNumberType: 1
      }
    ],
    ContactPerson: {
      EmailAddress: '',
      FirstName: '',
      LastName: '',
      JobTitle: [
        {
          CultureName: 'fr-FR',
          Text: ''
        }
      ],
      PhoneNumbers: [
        {
          LineNumber: '',
          Extension: '',
          PhoneNumberType: 1
        }
      ]
    },
    Code: '',
    Name: [
      {
        CultureName: '',
        Text: ''
      },
      {
        CultureName: '', 
        Text: ''
      }
    ],
    Description: [
      {
        CultureName: '',
        Text: ''
      },
      {
        CultureName: '',
        Text: ''
      }
    ]
  }
  

  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    public cooperative: CooperativeService,
    public router: Router,
    private general: GeneralService,

    ) {
      // this.getTontineTypeList()
      this.getCooperativeList();
      console.log('Cooop Deta: ', this.cooperativeDetails);
      this.getCountriesList();
    }

  ngOnInit() {

  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }



  viewBtn(cooperativeId) {
    sessionStorage.setItem("viewedCooperativeId",cooperativeId);
    this.router.navigate(['/cooperative/info'],{
    });
  }

  langueBtn() {
    this.enOn = !this.enOn;
  }

  async getTownById(Id) {
    
  }

  async getCountriesList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getCountryList())
    .toPromise()
    .then(async (res) => {
      this.countries = res as CountryModel[];
      console.log('Country: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }
   
  async getTown(country) {
    console.log(country.target.value);
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.general.getTownsList(country.target.value))
    .toPromise()
    .then(async (res) => {
      this.towns = res as TownModel[];
      console.log('Town: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  deleteTontineType(Id){

  }

  editTontineType(tontineType) {

  }

  updateTontineType(Id) {

  }

  createTontineType() {
    
  }

  async getCooperativeList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperativeList = res as CooperativeModel[];
      this.cooperativeList.forEach(element => {
        if(!element.EffectiveDate) {
          element.EffectiveDate = 'Unactive'          
        }
        if(element.Name[0]) {
          element.Name[0] = {
            CultureName: element.Name[0].CultureName + '',
            Text: element.Name[0].Text + '',
          }
        } else {
          element.Name[0] = {
            CultureName: ' ',
            Text: ' ',
          }
        }
        if(element.Name[1]) {
          element.Name[1] = {
            CultureName: element.Name[1].CultureName + '',
            Text: element.Name[1].Text + '',
          }
        } else {
          element.Name[1] = {
            CultureName: ' ',
            Text: ' ',
          }
        }

        if(element.Description[0]) {
          element.Description[0] = {
            CultureName: element.Description[0].CultureName + '',
            Text: element.Description[0].Text + '',
          }
        } else {
          element.Description[0] = {
            CultureName: ' ',
            Text: ' ',
          }
        }
        if(element.Description[1]) {
          element.Description[1] = {
            CultureName: element.Description[1].CultureName + '',
            Text: element.Description[1].Text + '',
          }
        } else {
          element.Description[1] = {
            CultureName: ' ',
            Text: ' ',
          }
        }
      });
      this.ngxService.stop();

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Authentification impossible');
    });
  }

  async error() {
    this.toastr.error('Action impossible', 'Erreur');

  }

  confirmChangeCooperativeStatus(Id, status) {
    Swal.fire({
      title: 'Changement de statut',
      text: 'Voulez vous changer le statut de la tontine sélectionnée ?',
      type: 'warning',
      confirmButtonText: 'Changer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Changement de statut annulé', 'error');
        } else if( status == false) {
          Swal.fire({
            text: 'Date d\'activation:',
            html: '<h5 class="mt-5">Veuillez choisir la date d\'activation de la tontine SVP</h5> <hr><input  id="tontineActivationDate" name="tontineActivationDate" ngModel [(ngModel)]="tontineActivationDate" type="date" class="swal2-input">',
          }).then(async (result) => {
            console.log('Result: ',  $('#tontineActivationDate').val());
            this.changeCooperativeStatus(Id,status, $('#tontineActivationDate').val());
            new Promise(function (resolve) {
              resolve([
                $('#tontineActivationDate').val(),
                console.log('Result: ',  $('#tontineActivationDate').val()),
              ])
            })
            if (result.value) {
              Swal.fire('Succès', 'Coopérative en cours de d \'activation', 'success');
            }
          });
         
        } else {
          await this.changeCooperativeStatus(Id,status, '2020-12-12');
          Swal.fire('Succès',' Coopérative en cours de désactivation', 'success');

        }
      });
  }

  confirmDeleteCooperative(Id) {
    Swal.fire({
      title: 'Confirmation de suppression',
      text: 'Voulez vous supprimer la coopérative sélectionnée',
      type: 'warning',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      showCloseButton: true,
      showCancelButton: true
    }).then(async (willDelete) => {
        if (willDelete.dismiss) {
          Swal.fire('Erreur', 'Suppression annulée', 'error');
        } else {
          await this.deleteCooperative(Id)
          Swal.fire('Succès',' coopérative en cours de suppression', 'success');

        }
      });
  }
 
  async changeCooperativeStatus(CoopId, status, date) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.cooperative.changeStatus(CoopId, status, date))
    .toPromise()
    .then((res) => {
      console.log('change Status details ', res);
      this.toastr.info('Chargement en cours', 'Info');
     if (!this.cooperativeDetails) {
      this.toastr.error('Ipossible de modifier cette coopérative veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.toastr.success('Données coopérative chargée avec succès', 'Success');     
      this.getCooperativeList();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.cooperativeListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
      console.log('An error occured ', err);
    });

   }

  async deleteCooperative(Id) {
    this.ngxService.start();
    this.toastr.info('Chargement en cours', 'Info');
    (await this.cooperative.deleteCooperative(Id))
    .toPromise()
    .then((res) => {
      
      console.log('res delete Coopérative ', res);
      this.toastr.info('Suppression effectuée avec succès', 'Info');
      this.getCooperativeList();
     if (this.cooperativeList.length === 0) {
      this.cooperativeListEmpty = true;
      this.toastr.error('Ipossible de  supprimer cette coopérative veuillez réessayer', 'Erreur');
      this.modalService.dismissAll();
    } else {
      this.cooperativeListEmpty = false;
      this.toastr.success('Coopérative supprimé avec succès', 'Success');
      this.resetCooperativeTable();
      this.modalService.dismissAll();
    }
     this.ngxService.stop();
    })
    .catch((err) => {
      this.cooperativeListEmpty = true;
      this.ngxService.stop();
      this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
      this.toastr.warning(err.error, 'Erreur');
      console.log('An error occured ', err);
    });
   }

   async resetCooperativeTable() {
    this.cooperativeList = [{
      Id: '',
      IsActive: true,
      EffectiveDate: '',
      TownId: '',
      Address: {
        City: '',
        CivicAddress: ''
      },
      PhoneNumbers: [
        {
          LineNumber: '',
          Extension: '',
          PhoneNumberType: 1
        }
      ],
      ContactPerson: {
        EmailAddress: '',
        FirstName: '',
        LastName: '',
        JobTitle: [
          {
            CultureName: 'fr-FR',
            Text: ''
          }
        ],
        PhoneNumbers: [
          {
            LineNumber: '',
            Extension: '',
            PhoneNumberType: 1
          }
        ]
      },
      Code: '',
      Name: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      Description: [
        {
          CultureName: '',
          Text: ''
        },
        {
          CultureName: '',
          Text: ''
        }
      ],
      Town: null
    }]
   }

  
   async getCoopDetails(CoopId) {
      this.ngxService.start();
      this.toastr.info('Chargement en cours', 'Info');
      (await this.cooperative.getCooperativeById(CoopId))
      .toPromise()
      .then((res) => {
        console.log('Get coop details ', res);
        this.toastr.info('Chargement en cours', 'Info');
        this.cooperativeDetails = res;
        console.log('Get coop details2 ', this.cooperativeDetails);
        if(!this.cooperativeDetails.Name[1].Text) {
          this.cooperativeDetails.Name[1].Text= ''
        }
        if(!this.cooperativeDetails.Description[1]) {
          this.cooperativeDetails.Description.push({
            'CultureName': 'en-EN',
            'Text':''
          })
        } else if(!this.cooperativeDetails.Description[1].Text) {
          this.cooperativeDetails.Description[1].Text= ''
        }
        if(!this.cooperativeDetails.EffectiveDate) {
          this.cooperativeDetails.EffectiveDate= ''
        }
       if (!this.cooperativeDetails) {
        this.toastr.error('Ipossible de modifier cette coopérative veuillez réessayer', 'Erreur');
        this.modalService.dismissAll();
      } else {
        this.toastr.success('Données coopérative chargée avec succès', 'Success');
        
      }
       this.ngxService.stop();
      })
      .catch((err) => {
        this.cooperativeListEmpty = true;
        this.ngxService.stop();
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
        this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
        console.log('An error occured ', err);
      });
  
     }

      async updateCoop(CoopId, form) {
        this.ngxService.start();
        this.toastr.info('Chargement en cours', 'Info');
        (await this.cooperative.updateCooperative(CoopId, form))
        .toPromise()
        .then((res) => {
          console.log('res create profile ', res);
          this.toastr.info('Modification en cours de traitement', 'Info');
          this.getCooperativeList();
         if (this.cooperativeList.length === 0) {
          this.cooperativeListEmpty = true;
          this.toastr.error('Ipossible de modifier cette coopérative veuillez réessayer', 'Erreur');
          this.modalService.dismissAll();
        } else {
          this.cooperativeListEmpty = false;
          this.toastr.success('Cooperative modifié avec succès', 'Success');
          this.resetCooperativeTable();
          this.modalService.dismissAll();
        }
         this.ngxService.stop();
        })
        .catch((err) => {
          this.cooperativeListEmpty = true;
          this.ngxService.stop();
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Chargement impossible');
          this.toastr.error('Veuillez vérifier votre connexion internet', 'Erreur');
          console.log('An error occured ', err);
        });

     }
  
}


