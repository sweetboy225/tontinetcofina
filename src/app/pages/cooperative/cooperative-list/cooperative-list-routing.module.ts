import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CooperativeListComponent} from './cooperative-list.component';


const routes: Routes = [
  {
    path: '',
    component: CooperativeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CooperativeListRoutingModule { }
