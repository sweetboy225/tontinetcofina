import { NgxUiLoaderService } from 'ngx-ui-loader';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';


import { DatePipe } from '@angular/common';
import { SecurityModel } from 'src/app/shared/models/security.model';
import { RoleModel } from 'src/app/shared/models/role.model';
import { PermissionSetsModel } from 'src/app/shared/models/permissionSets.model';
import { SecurityService } from 'src/app/shared/services/security.service';

import 'sweetalert2/src/sweetalert2.scss';
import Swal from 'sweetalert2';
import { GeneralService } from 'src/app/shared/services/general.service';
import { CountryModel } from 'src/app/shared/models/country.model';
import { TownModel } from 'src/app/shared/models/town.model';
import { TontineService } from 'src/app/shared/services/tontine.service';
import { MemberService } from 'src/app/shared/services/member.service';

import { Router, ActivatedRoute } from '@angular/router';
import { CooperativeModel } from 'src/app/shared/models/cooperative.model';
import { CooperativeService } from 'src/app/shared/services/cooperative.service';

@Component({
  selector: 'app-member-add',
  templateUrl: './member-add.component.html',
  styleUrls: ['./member-add.component.scss']
})
export class MemberAddComponent implements OnInit {
  asEdit= false;
  form: any;

  public isSubmit: boolean;

  closeResult = '';
  cooperatives: CooperativeModel[];
  cooperativeId= '';
  towns: TownModel[];
  userId= '';
  usersListEmpty = true;
  CultureNameEn= 'en-US';
  CultureNameFr= 'fr-FR';
  memberId= '';
  enOn= false;
  constructor(
    private modalService: NgbModal,
    public toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    public datepipe: DatePipe,
    private cooperative: CooperativeService,
    private member: MemberService,
    private router: Router,
    private activateRouter:ActivatedRoute
    ) {
      this.userId = localStorage.getItem('userId');
      this.getCooperativesList();
      this.isSubmit = false;
    }

  ngOnInit() {
    this.memberId = sessionStorage.getItem('memberId')
    console.log('Coop Id: ', this.memberId ) 
  }


  open(content) {
    this.modalService.open(content, {size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('dismissed');
    });
  }

  editBtn() {
    this.asEdit = true;
  }

  langueBtn() {
    this.enOn = !this.enOn;
  }

  async getTownById(Id) {
    
  }

  async getCooperativesList() {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.cooperative.getCooperativeList())
    .toPromise()
    .then(async (res) => {
      this.cooperatives = res as CooperativeModel[];
      console.log('Cooperatice: ', res)
      this.ngxService.stop();
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.asEdit = false;
      this.toastr.error('Veuillez réessayer', 'Connexion impossible');
    });
  }
   
  async createMember( memberForm) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    console.log('Create: ', memberForm + ' ' + this.cooperativeId);
    (await this.member.createMember(this.cooperativeId, memberForm))
    .toPromise()
    .then((res) => {
     console.log(res);
     this.toastr.success('Coopérative crée veuillez patienter', 'Success');
     this.ngxService.stop();    
     this.router.navigate(['/member/list'], { skipLocationChange: true });
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });

  }
  async resetForm() {

  }
}

