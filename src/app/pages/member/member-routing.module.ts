import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        loadChildren: () => import('./member-add/member-add.module').then(m => m.MemberAddModule)
      },
      {
        path: 'list',
        loadChildren: () => import('./member-list/member-list.module').then(m => m.MemberListModule)
      }/* ,
      {
        path: 'member',
        loadChildren: () => import('./member-member/member-member.module').then(m => m.MemberMemberModule)
      },
      {
        path: 'details',
        loadChildren: () => import('./member-details/member-details.module').then(m => m.MemberDetailsModule)
      } */
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
  