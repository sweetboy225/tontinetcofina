import { Component, OnInit } from '@angular/core';

import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { SecurityService } from './../../../shared/services/security.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss']
})
export class AuthSignupComponent implements OnInit {
  remember: boolean; 
  email: string;
  password: string;
  constructor(
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private security: SecurityService,
    private router: Router

  ) {

   }

  ngOnInit() {
  }

  rememberData(form) {
  }

  formVerification() {

  }
  

  async register(form) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.registerUser(form))
    .toPromise()
    .then((res) => {
      // console.log(res);
      this.email = form.Email;
      this.password = form.Password;
      this.resetPassword(form);
      if(this.remember){
        localStorage.setItem('userEmail', form.Email);
        localStorage.setItem('userPassword', form.Password);
      }
     this.toastr.success('Inscription effectuée veuillez patienter', 'Success');
     this.ngxService.stop();     
    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err.error);
      this.toastr.error('Veuillez vérifier les données saisies', 'Inscription impossible');
    });
  }

  async resetPassword(form) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.forgotPassword(form))
    .toPromise()
    .then((res) => {
      this.ngxService.stop();
      this.toastr.success(res, 'Email envoyé');
      this.router.navigate(['/auth/change-password'], { skipLocationChange: true });

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn(err);
      if (err.status == 400) {
      this.toastr.error('Veuillez vérifier les données saisies', 'Erreur');
      } else {
      this.toastr.error(err.error, 'Erreur');
    }
    });
  }
}
