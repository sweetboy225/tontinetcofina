import { Router } from '@angular/router';
import { SecurityService } from './../../../shared/services/security.service';
import { Component, OnInit } from '@angular/core';
import { FormsModule,  FormGroup, FormControl, Validators   } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth-signin.component.html',
  styleUrls: ['./auth-signin.component.scss']
})

export class AuthSigninComponent implements OnInit {

  userEmails = new FormGroup({
  primaryEmail: new FormControl('',[
  Validators.required,
  Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
  secondaryEmail: new FormControl('', [
  Validators.required,
  Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')])
  });

  remember: false; 
  Email= '';
  Password= '';
  constructor(
    private ngxService: NgxUiLoaderService,
    private security: SecurityService,
    private router: Router,
    private toastr: ToastrService
    ) {
      if(localStorage.getItem('userEmail')){
        this.Email= localStorage.getItem('userEmail')
      }
      if(localStorage.getItem('userPassword')){
        this.Password= localStorage.getItem('userPassword')
      }
      console.log('Email: ', localStorage.getItem('userEmail'));
      console.log('Password: ', localStorage.getItem('userPassword'));
     }

  ngOnInit() {
  }

  checkForm(form) {

  }

  async login(form) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.userLogin(form))
    .toPromise()
    .then((res) => {
     if(this.remember){
      localStorage.setItem('userEmail', form.Email);
      localStorage.setItem('userPassword', form.Password);
     }
     this.toastr.success('Connexion réussie veuillez patienter', 'Success');
     localStorage.setItem('userId', res.Id);
     localStorage.setItem('firstName', res.FirstName);
     localStorage.setItem('lastName', res.LastName);
     localStorage.setItem('userSecurityStamp', res.SecurityStamp);
     this.ngxService.stop();
     this.toastr.success('Connexion réussi', 'Success');
     this.router.navigate(['/dashboard/default'], { skipLocationChange: true });

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn('une erreur', err)
      this.toastr.error(err.error, 'Erreur');
    });
  }

  testToast(){
    console.log('Test toast');
    this.toastr.success('Veuillez vérifier les données saisies', 'Erreur');
  }
}
