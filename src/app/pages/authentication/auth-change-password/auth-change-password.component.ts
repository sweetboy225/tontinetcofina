import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SecurityService } from 'src/app/shared/services/security.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-auth-change-password',
  templateUrl: './auth-change-password.component.html',
  styleUrls: ['./auth-change-password.component.scss']
})
export class AuthChangePasswordComponent implements OnInit {
  wrongCode = true
  Email: any;
  constructor(
    private ngxService: NgxUiLoaderService,
    private security: SecurityService,
    public router: Router,
    private toastr: ToastrService

  ) {
    // this.Email = localStorage.getItem('userEmail');
  }

  ngOnInit() {
  }

  async resetPassword(form) {
    console.log('Form is: ', form);
    if(form.NewPassword !== form.ConfirmNewPassword) {
      this.toastr.error('les mot de passes saisies sont différents', 'Erreur');
    } else {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.resetPassword(form))
    .toPromise()
    .then((res) => {
    console.log('success', res);
    localStorage.setItem('userEmail', form.Email);
    this.ngxService.stop();
    this.toastr.success('Veuillez vous connecter', 'Réinitialisation réussie');
    this.router.navigate(['/auth/signin'], { skipLocationChange: true });
    })
    .catch((err) => {
      console.warn('error', err)
      this.wrongCode = false;
      this.ngxService.stop();
      if (err.status == 400) {
        this.toastr.error('Modification impossible veuillez contacter l administrateur', 'Erreur');
      } else {
        this.toastr.error(err.error, 'Erreur');
      }
    });
  }
  }
  checkPassword() {
    console.log('Password checked');
  }

}
