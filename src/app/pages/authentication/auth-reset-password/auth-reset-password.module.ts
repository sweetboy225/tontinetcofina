import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthResetPasswordRoutingModule } from './auth-reset-password-routing.module';
import { AuthResetPasswordComponent } from './auth-reset-password.component';
import { FormsModule } from '@angular/forms';
import { NgxUiLoaderModule } from 'ngx-ui-loader';

@NgModule({
  imports: [
    CommonModule,
    AuthResetPasswordRoutingModule,
    FormsModule,
    NgxUiLoaderModule,
  ],
  declarations: [AuthResetPasswordComponent]
})
export class AuthResetPasswordModule { }
