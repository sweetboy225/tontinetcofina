import { SecurityService } from './../../../shared/services/security.service';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth-reset-password',
  templateUrl: './auth-reset-password.component.html',
  styleUrls: ['./auth-reset-password.component.scss']
})
export class AuthResetPasswordComponent implements OnInit {

  constructor(
    private ngxService: NgxUiLoaderService,
    private security: SecurityService,
    public router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
  }

  async resetPassword(form) {
    this.ngxService.start();
    this.toastr.info('Traitement en cours', 'Info');
    (await this.security.forgotPassword(form))
    .toPromise()
    .then((res) => {
      localStorage.setItem('userEmail', form.Email);
      this.ngxService.stop();
      this.toastr.success(res, 'Email envoyé');
      this.router.navigate(['/auth/change-password'], { skipLocationChange: true });

    })
    .catch((err) => {
      this.ngxService.stop();
      console.warn(err);
      if (err.status == 400) {
      this.toastr.error('Veuillez vérifier les données saisies', 'Erreur');
    } else {
      this.toastr.error(err.error, 'Erreur');
    }
    });
  }
}
