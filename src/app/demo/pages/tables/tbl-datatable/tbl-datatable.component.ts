import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tbl-datatable',
  templateUrl: './tbl-datatable.component.html',
  styleUrls: ['./tbl-datatable.component.scss']
})
export class TblDatatableComponent implements OnInit {
  dtExportButtonOptions: any = {};

  constructor() { }
 
  ngOnInit() {
    this.dtExportButtonOptions = {
      //ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Code',
        data: 'name'
      }, {
        title: 'Nom',
        data: 'position'
      }, {
        title: 'Prénoms',
        data: 'office'
      }, {
        title: 'Contact',
        data: 'age'
      }, {
        title: 'Date de naissance',
        data: 'date'
      }, {
        title: 'Salary',
        data: 'salary'
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    };
  }

}
