import { environment } from './../../../environments/environment';
import { SecurityModel } from './../models/security.model';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RoleModel } from '../models/role.model';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

formData: SecurityModel;
list: SecurityModel[];
token: string;
  constructor(
    private http: HttpClient
  ) {
  }

  async userLogin(authForm) {
    console.log('Auth ss', authForm);
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    return this.http.post<SecurityModel>( environment.BaseUrl + '/security/users/login', authForm,
    {responseType: 'json', headers});
  }

  async forgotPassword(forgotForm) {
    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<string>(environment.BaseUrl + '/security/users/forgotpassword', forgotForm,
    {responseType: 'json', headers});

  }


  async resetPassword(resetForm) {
    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<SecurityModel>(environment.BaseUrl + '/security/users/resetpassword', resetForm,
    {responseType: 'json', headers});

  }


  async getUserById(userId) {
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<SecurityModel>(environment.BaseUrl + '/security/users/' + userId,
    {responseType: 'json', headers});
  }

  async deleteUser(userId) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<SecurityModel>(environment.BaseUrl + '/security/users/' + userId,
    {responseType: 'json', headers});
  }

  async getAllUsers() {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get(environment.BaseUrl + '/security/users', {headers});

  }

  async getToken() {
    const userId = localStorage.getItem('userId');
    const userSecurityStamp = localStorage.getItem('userSecurityStamp');
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    const body = {
      securityStamp: userSecurityStamp,
    };
    const response = await this.http.post<SecurityModel>(environment.BaseUrl +'/security/users/' + userId + '/token', body,
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }

  async getRolesList() {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return await this.http.get<RoleModel[]>(environment.BaseUrl + '/security/roles/',
    {responseType: 'json', headers});
  }

  async createRole(roleName) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body = { };
    return this.http.post<RoleModel>(environment.BaseUrl + '/security/roles?name=' + roleName, body,
    {responseType: 'json', headers});

  }

  async addRole2User(roleId, userId) {
    this.token = await this.getToken();
    console.log('Add role 2 user: ', roleId , ' ', userId, ' ', environment.BaseUrl + '/security/roles/' + roleId + '/users/' + userId  )
    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body = { };
    return this.http.post<RoleModel>(environment.BaseUrl + '/security/roles/' + roleId + '/users/' + userId, body,
     {responseType: 'json', headers});

  }

  async addClaims2Role(roleId, claimType, permissionLevel) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body = {
      'claimType': claimType,
      'permissionLevel': permissionLevel
     };
    return this.http.post<RoleModel>(environment.BaseUrl + '/security/roles/' + roleId + '/claims/', body,
     {responseType: 'json', headers});

  }

  async getClaimsType() {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const response = await this.http.get<RoleModel>(environment.BaseUrl + '/security/roles/',
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }

  async registerUser(form) {
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    return this.http.post<SecurityModel>(environment.BaseUrl + '/security/users/register', form,
    {responseType: 'json', headers});
  }

  async updateUser(UserId, form) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.put<SecurityModel>(environment.BaseUrl + '/security/users/' + UserId, form,
    {responseType: 'json', headers});
  }

  async updateRole(RoleId, RoleName) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    const body = {
      'Name': RoleName,
    };
    return this.http.put<SecurityModel>(environment.BaseUrl + '/security/roles/' + RoleId, body,
    {responseType: 'json', headers});
  }

  async changeUserRole(RoleId, UserId, RoleName) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    const body = {
      'Name': RoleName,
    };
    return this.http.put<SecurityModel>(environment.BaseUrl + '/security/uses/' + UserId + '?roleId=' + RoleId, body,
    {responseType: 'json', headers});
  }

  async DeleteUser(UserId) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    const body = {};
    return this.http.delete<SecurityModel>(environment.BaseUrl + '/security/users/' + UserId,
    {responseType: 'json', headers});
  }

  async DeleteRole(RoleId) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    const body = {};
    return this.http.delete<SecurityModel>(environment.BaseUrl + '/security/roles/' + RoleId,
    {responseType: 'json', headers});
  }
  

  async DeleteRoleUser(RoleId, UserId) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    const body = {};
    return this.http.delete<boolean>(environment.BaseUrl + '/security/roles/' + RoleId + '/users/' + UserId,
    {responseType: 'json', headers});
  }

  logOut() {

  }




}
