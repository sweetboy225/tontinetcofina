

import { environment } from './../../../environments/environment';
import { CooperativeModel } from './../models/cooperative.model';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RoleModel } from '../models/role.model';
import { CountryModel } from '../models/country.model';
import { MemberModel } from '../models/member.model';
import { SearchMemberModel } from '../models/searchMember.model';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

formData: MemberModel;
list: MemberModel[];
token: string;
  constructor(
    private http: HttpClient
  ) {
  }


  async getMembersByCoopId(CoopId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<MemberModel[]>(environment.BaseUrl + '/cooperative/'+ CoopId + '/members/',
    {responseType: 'json', headers});

  }

  async getMemberById(memberId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<MemberModel>(environment.BaseUrl + '/members/'+ memberId,
    {responseType: 'json', headers});

  }


  async createMember(CoopId, memberForm) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<CountryModel>(environment.BaseUrl + '/cooperative/' + CoopId + '/members', memberForm,
    {responseType: 'json', headers});

  }

  async updateMember(memberId, memberForm) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.put<CountryModel>(environment.BaseUrl + '/members/' + memberId, memberForm,
    {responseType: 'json', headers});

  }

  async changeMemberStatus(memberId, memberStatus) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body = {
        IsActive: !memberStatus
    }
  console.log('Member status change : ', memberId + ' ' + memberStatus + ' '+ body )
    return this.http.put<CountryModel>(environment.BaseUrl + '/members/' + memberId , body,
    {responseType: 'json', headers});

  }

  async deleteMember(memberId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<CountryModel>(environment.BaseUrl + '/members/' + memberId,
    {responseType: 'json', headers});

  }
  
  async searchMember(cooperativeId, CritriaType, FirstName, LastName, MemberCode, PageNumber, PageSize) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<SearchMemberModel>(environment.BaseUrl + '/cooperative/' + cooperativeId + '/members/search?CritriaType=' + CritriaType + '?FirstName=' + FirstName + '?LastName=' + LastName + '?MemberCode=' + MemberCode + '?PageNumber=' + PageNumber + '?PageSize=' + PageSize,
    {responseType: 'json', headers});
  }

  async getToken() {
    const userId = localStorage.getItem('userId');
    const userSecurityStamp = localStorage.getItem('userSecurityStamp');
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    const body = {
      securityStamp: userSecurityStamp,
    };
    const response = await this.http.post<CooperativeModel>(environment.BaseUrl +'/security/users/' + userId + '/token', body,
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }

}
