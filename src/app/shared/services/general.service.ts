

import { environment } from './../../../environments/environment';
import { CooperativeModel } from './../models/cooperative.model';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RoleModel } from '../models/role.model';
import { CountryModel } from '../models/country.model';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

formData: CooperativeModel;
list: CooperativeModel[];
token: string;
  constructor(
    private http: HttpClient
  ) {
  }


  async getCountryList() {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get(environment.BaseUrl + '/countries', {headers});

  }

  async getCountryById(countryId) {
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CountryModel>(environment.BaseUrl + '/countries/' + countryId,
    {responseType: 'json', headers});
  }

  async createCountry(country) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<CountryModel>(environment.BaseUrl + '/countries', country,
    {responseType: 'json', headers});

  }
  
  async getTownsList(countryId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get(environment.BaseUrl + '/countries/' + countryId + '/towns', {headers});

  }
  async getTownsById(townId) {
    this.token = await this.getToken();
    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CountryModel>(environment.BaseUrl + '/towns' + townId,
     {responseType: 'json', headers});

  }

  async getToken() {
    const userId = localStorage.getItem('userId');
    const userSecurityStamp = localStorage.getItem('userSecurityStamp');
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    const body = {
      securityStamp: userSecurityStamp,
    };
    const response = await this.http.post<CooperativeModel>(environment.BaseUrl +'/security/users/' + userId + '/token', body,
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }


}
