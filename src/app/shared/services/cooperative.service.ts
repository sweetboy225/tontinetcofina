

import { environment } from './../../../environments/environment';
import { CooperativeModel } from './../models/cooperative.model';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RoleModel } from '../models/role.model';
import { CountryModel } from '../models/country.model';
import { TontineTypeModel } from '../models/tontineType.model';
import { MemberModel } from '../models/member.model';
import { SearchMemberModel } from '../models/searchMember.model';
import { SecurityModel } from '../models/security.model';
import { TontineModel } from '../models/tontine.model';

@Injectable({
  providedIn: 'root'
})
export class CooperativeService {

formData: CooperativeModel;
list: CooperativeModel[];
token: string;
  constructor(
    private http: HttpClient
  ) {
  }

  async getCooperativeList() { 
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CooperativeModel[]>(environment.BaseUrl + '/cooperative/',
    {responseType: 'json', headers});
  }

  async createCooperative(cooperativeForm) {
    console.log('Cooperative form ', cooperativeForm)
    const body = {
    TownId: cooperativeForm.TownId,
    Code: cooperativeForm.Code,
    Name: [ 
      {
        CultureName: "fr-FR",
        Text: cooperativeForm.NameText1
      }
    ]
  }
  console.log('Body: ', body );
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<CooperativeModel>(environment.BaseUrl + '/cooperative/', body,
    {responseType: 'json', headers});
  }

  async deleteTontine(tontineId) {
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<CooperativeModel>(environment.BaseUrl + '/tontines/' + tontineId,
    {responseType: 'json', headers});
  }


  async getCooperativeById(cooperativeId) {
    console.log('Test 225');
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CooperativeModel>(environment.BaseUrl + '/cooperative/' + cooperativeId,
    {responseType: 'json', headers});
  }

  async updateCooperative(cooperativeId, cooperativeForm) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });
    
    const body = {
      TownId: cooperativeForm.TownId,
      Address: {
        City: cooperativeForm.Address.City,
        CivicAddress: cooperativeForm.Address.CivicAddress
      },
      PhoneNumbers: [
        {
          LineNumber: cooperativeForm.PhoneNumbers[0].LineNumber,
          Extension: cooperativeForm.PhoneNumbers[0].Extension,
          PhoneNumberType: cooperativeForm.PhoneNumbers[0].PhoneNumberType
        }
      ],
      ContactPerson: {
        EmailAddress: cooperativeForm.ContactPerson.EmailAddress,
        FirstName: cooperativeForm.ContactPerson.FirstName,
        LastName: cooperativeForm.ContactPerson.LastName,
        JobTitle: [
          {
            CultureName: 'fr-FR',
            Text: cooperativeForm.ContactPerson.JobTitle[0].Text
          },
          {
              CultureName: 'en-US',
              Text: ' '
          }
        ],
        PhoneNumbers: [
          {
            LineNumber: cooperativeForm.ContactPerson.PhoneNumbers[0].LineNumber,
            Extension: cooperativeForm.ContactPerson.PhoneNumbers[0].Extension,
            PhoneNumberType: 1
          }
        ]
      },
      Code: cooperativeForm.Code,
      Name: [
        {
          CultureName: 'fr-FR',
          Text: cooperativeForm.Name[0].Text
        },
        {
          CultureName: 'en-US',
          Text: cooperativeForm.Name[1].Text
        }
      ],
      Description: [
          {
              CultureName: 'fr-FR',
              Text: cooperativeForm.Description[0].Text
            },
            {
              CultureName: 'en-US',
              Text: cooperativeForm.Description[1].Text
            }
      ]
    }
    console.log('Cooperative Update body: ', body)
    return this.http.put<CooperativeModel>(environment.BaseUrl + '/cooperative/' + cooperativeId, body,
    {responseType: 'json', headers});
  }

  async deleteCooperative(cooperativeId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<CooperativeModel>(environment.BaseUrl + '/cooperative/' + cooperativeId,
    {responseType: 'json', headers});
  }

  async getMembersByCooperative(cooperativeId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CooperativeModel>(environment.BaseUrl + '/cooperative/' + cooperativeId + '/members',
    {responseType: 'json', headers});
  }

  async createMember(cooperativeId, memberForm) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.put<MemberModel>(environment.BaseUrl + '/cooperative/' + cooperativeId + '/members/', memberForm,
    {responseType: 'json', headers});
  }

  
  async getToken() {
    const userId = localStorage.getItem('userId');
    const userSecurityStamp = localStorage.getItem('userSecurityStamp');
    console.log('S Stamp 1: ', userSecurityStamp );
    console.log('S Stamp 2: ',localStorage.getItem('userSecurityStamp'));
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    const body = {
      securityStamp: userSecurityStamp,
    };
    const response = await this.http.post<string>(environment.BaseUrl +'/security/users/' + userId + '/token', body,
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }

  async changeStatus(cooperativeId: string, status: boolean, date: Date) {
    console.log('Change coop header: ', cooperativeId, status)

    this.token = await this.getToken();
    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    }); 
    const body = {
      Active: !status,
      EffectiveDate: date
    }
    console.log('Change coop body: ', body)
    return this.http.put<boolean>(environment.BaseUrl + '/cooperative/' + cooperativeId + '/status', body,
    {responseType: 'json', headers});
  }
}

