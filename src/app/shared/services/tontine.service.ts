

import { environment } from './../../../environments/environment';
import { CooperativeModel } from './../models/cooperative.model';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { RoleModel } from '../models/role.model';
import { CountryModel } from '../models/country.model';
import { TontineTypeModel } from '../models/tontineType.model';
import { TontineModel } from '../models/tontine.model';
import { MemberModel } from '../models/member.model';
import { ShareModel } from '../models/share.model';

@Injectable({
  providedIn: 'root'
})
export class TontineService {

formData: CooperativeModel;
list: CooperativeModel[];
token: string;
  constructor(
    private http: HttpClient
  ) {
  }
  
 
  async createTontine(tontineForm, coopId) {
    
    const body = {
      Code: tontineForm.Code,
      Name: [ 
        {
          CultureName: "fr-FR",
          Text: tontineForm.NameText1
        },
        { 
          CultureName: "en-US",
          Text: tontineForm.NameText2 + ""
        }
      ],
      Type: tontineForm.Type
    }
    
    this.token = await this.getToken();
    console.log('Body tontine: ', body );
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.post<TontineModel>(environment.BaseUrl + '/cooperative/' + coopId +'/tontines', body,
    {responseType: 'json', headers});
  }

  async getTontinesByCooperative(cooperativeId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<TontineModel[]>(environment.BaseUrl + '/cooperative/' + cooperativeId + '/tontines',
    {responseType: 'json', headers});
  }

  async getTontineById(tontineId) { 
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<TontineModel>(environment.BaseUrl + '/tontines/' + tontineId,
    {responseType: 'json', headers});
  }

  async updateTontine(tontineId, tontineForm: TontineModel) {
    console.log('Update tontine form: ', tontineForm )
    this.token = await this.getToken();
    const body = {
      Code: tontineForm.Code,
      Name: [ 
        {
          CultureName: "fr-FR", 
          Text: tontineForm.Name[0].Text
        },
        { 
          CultureName: "en-US",
          Text: tontineForm.Name[1].Text
        }
      ],
      Description: [
        {
          CultureName: "fr-FR",
          Text: tontineForm.Description[0].Text
        },
        {
          CultureName: "en-US",
          Text: tontineForm.Description[1].Text
        }
      ],
      MembershipTicketFee: tontineForm.MembershipTicketFee,
      ContributionsAmount: tontineForm.ContributionsAmount,
      EndDate: tontineForm.EndDate,
      BeginDate: tontineForm.BeginDate,
      Frequency: tontineForm.Frequency.Value,
      Type: tontineForm.Type.Value
    }
    console.log('Update tontine Body: ', body)
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.put<CountryModel>(environment.BaseUrl + '/tontines/' + tontineId, body,
    {responseType: 'json', headers});
  }

  async deleteTontine(tontineId) {
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<Boolean>(environment.BaseUrl + '/tontines/' + tontineId,
    {responseType: 'json', headers});
  }


  async getParticipantByTontine(tontineId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<MemberModel[]>(environment.BaseUrl + '/tontines/' + tontineId + '/participants',
    {responseType: 'json', headers});
  }

  async removeParticipants(tontineId, memberId) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.delete<CountryModel>(environment.BaseUrl + '/tontines/' + tontineId + '/participants?MemberId=' + memberId,
    {responseType: 'json', headers});
  }

  async addParticipants2Tontine(tontineId, memberId, part) {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body = {
      MemberId: memberId,
      Part: part
    }
    console.log('Body add participant: ', body)

    return this.http.post<boolean>(environment.BaseUrl + '/tontines/' + tontineId + '/participants/', body,
    {responseType: 'json', headers});
  }

  async getAccountByTontine(tontineId) {
    
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<CountryModel>(environment.BaseUrl + '/tontines/' + tontineId,
    {responseType: 'json', headers});
  }

  async getTontineType() {
    this.token = await this.getToken();

    // server
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    return this.http.get<TontineTypeModel[]>(environment.BaseUrl + '/tontines/types',
    {responseType: 'json', headers});

  }
  
  async changeTontineStatus(tontineId, status, date) {
    this.token = await this.getToken();
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token
    });

    const body ={ 
      Active: !status,
      EffectiveDate: date
    }

    console.log('Change status body: ', body)
    return this.http.put<boolean>(environment.BaseUrl + '/tontines/' + tontineId + '/status/',body,
    {responseType: 'json', headers});

  }

  async getTontineByCoop(CoopId) {
    
      this.token = await this.getToken();
      const headers = new  HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      });
  
      return this.http.get<CountryModel>(environment.BaseUrl + '/cooperative/' + CoopId + '/tontines/',
      {responseType: 'json', headers});
    }
  
    async generateShares(tontineId, daysOfWeek) {
      this.token = await this.getToken();
  
      // server
      const headers = new  HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      });
      
      const body ={
        DayOfWeek: daysOfWeek
      } 
      return this.http.post<boolean>(environment.BaseUrl + '/tontines/' + tontineId + '/shares/generate',body,
      {responseType: 'json', headers});
  
    }

    async clearShares(tontineId) {
      this.token = await this.getToken();
  
      // server
      const headers = new  HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      });

      return this.http.post<boolean>(environment.BaseUrl + '/tontines/' + tontineId + '/shares/generate',
      {responseType: 'json', headers});
  
    }

    async getAllShares(tontineId) {
      this.token = await this.getToken();
  
      // server
      const headers = new  HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      });

      return this.http.get<ShareModel[]>(environment.BaseUrl + '/tontines/' + tontineId + '/shares/generate',
      {responseType: 'json', headers});
  
    }

  async getToken() {
    const userId = localStorage.getItem('userId');
    const userSecurityStamp = localStorage.getItem('userSecurityStamp');
    const headers = new  HttpHeaders({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    });

    const body = {
      securityStamp: userSecurityStamp,
    };
    const response = await this.http.post<CooperativeModel>(environment.BaseUrl +'/security/users/' + userId + '/token', body,
    {responseType: 'json', headers})
    .toPromise();
    return response.toString();
  }


}
