import { environment } from './../../../environments/environment';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { CountryModel } from '../models/country.model'
import { TownModel } from '../models/town.model';
@Injectable({
    providedIn: 'root'
  })


export class GeneralService {
    
    constructor(
        private http: HttpClient
    ) {

    }

    async GetAllCountries() {
        const headers = new  HttpHeaders({
          Accept: 'application/json',
          'Content-Type': 'application/json',
        });
    
        return this.http.get<CountryModel>( environment.BaseUrl + '/countries',
        {responseType: 'json', headers});
    
    }

    async GetCountryById(Id) {
        const headers = new  HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
          });
      
          return this.http.get<CountryModel>( environment.BaseUrl + '/countries/'+ Id ,
          {responseType: 'json', headers});
  
    }

    async CreateCountry(CountryForm) {
        const headers = new  HttpHeaders({
          Accept: 'application/json',
          'Content-Type': 'application/json',
        });
    
        return this.http.post<CountryModel>( environment.BaseUrl + '/countries', CountryForm,
        {responseType: 'json', headers});
    
    }

    async GetTownsbyCountryId(Id) {
        const headers = new  HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
          });
      
          return this.http.get<TownModel>( environment.BaseUrl + '/countries/'+ Id + 'towns' ,
          {responseType: 'json', headers});

    }

    async AddTown2Country(Id, TownForm) {
        const headers = new  HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
          });
      
          return this.http.post<TownModel>( environment.BaseUrl + '/countries/'+ Id + 'towns', TownForm,
          {responseType: 'json', headers});
    }

    async GetTownsyById(Id) {
        const headers = new  HttpHeaders({
            Accept: 'application/json',
            'Content-Type': 'application/json',
          });
      
          return this.http.get<TownModel>( environment.BaseUrl + '/towns/'+ Id ,
          {responseType: 'json', headers});
    }
}