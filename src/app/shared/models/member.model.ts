export class MemberModel {
    Id: string;
    IsActive: boolean;
    EffectiveDate: string;
    Firstname: string;
    Lastname: string;
    MemberCode: string;
    PhoneNumber: string;
    BirthDate: string
} 