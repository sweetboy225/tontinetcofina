export class ShareModel 
  {
    id: string;
    tontineId: string;
    amountTaked: number;
    effectiveDate: string;
    index: 0;
    isClosed: true;
    takeByMember: {
      id: string;
      lastName: string;
      firstName: string
    };
    takedAt: string
}