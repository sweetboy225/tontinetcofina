import { data } from './data.model';

export class CountryModel {
    Id: string;
    Code: string;
    Name: data[]; 
    Description: data[];
}
