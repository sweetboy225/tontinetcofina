import { data } from './data.model';
import { data2 } from './data2.model';

export class TontineModel  {
  Id: string;
  Name: data[];
  Code: string;
  MembershipTicketFee: number;
  Description: data[];
  ContributionsAmount: number;
  Frequency: data2;
  Type: data2;
  IsActive: boolean;
  EffectiveDate: string;
  BeginDate: string;
  EndDate: string;

}
