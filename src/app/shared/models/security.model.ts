import { RoleModel } from './role.model';
import { PermissionSetsModel } from './permissionSets.model';
export class SecurityModel {
    Id: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Username: string;
    ClientId: string;
    GroupId: string;
    CenterId: number;
    Language: number;
    SecurityStamp: string;
    Roles: RoleModel[];
    PhoneNumber: string;
    PermissionSets: PermissionSetsModel[];
  }
