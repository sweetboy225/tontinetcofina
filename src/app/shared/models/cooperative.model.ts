import { data } from './data.model';
import { contactData } from './contactData.model';
import { TownModel } from './town.model';

export class CooperativeModel {
    Id: string;
    IsActive: boolean;
    EffectiveDate: string;
    TownId: string;
    Address: {
      City: string;
      CivicAddress: string
    };
    PhoneNumbers: contactData[];
    ContactPerson: {
      EmailAddress: string;
      FirstName: string;
      LastName: string;
      JobTitle: data[];
      PhoneNumbers: contactData[];
    };
    Code: string;
    Name: data[];
    Description: data[];
    Town: TownModel;
  }