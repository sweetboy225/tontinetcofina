import { MemberModel } from './member.model';

export class SearchMemberModel {
    Members:  MemberModel[];
    PageNumber: number;
    TotalPages: number;
    TotalMembersCount: number
}
